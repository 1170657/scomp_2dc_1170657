#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main() {
	
	int fd[2];
	
	pid_t p;
	
	pipe(fd);
	
	p = fork();
	
	if(p != 0) {
	
		char lido[500];
	
		close(fd[1]);
	
		if(read(fd[0], lido, 500) == -1) {
			exit(-1);
		}
	
		close(fd[0]);
	
		int status;
		wait(&status);
		
		if(!WIFEXITED(status)) {
			exit(1);
		}
	
		printf("Ordenado: %s\n", lido);	
	
	} else if(p == 0) { 
		
		close(fd[0]);
		dup2(fd[1],1);
		close(fd[1]);
		
		execlp("sort", "sort", "fx.txt", (char *) NULL);
		exit(1);

	} else if(p == -1) {
		exit(-1);
	} 
	
return 0;
}
		
	
