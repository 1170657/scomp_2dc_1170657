#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>

int main(void) {
	
    srand(time(NULL));
    
	int status;
	
	int vec1[1000];
	int vec2[1000];
	
	int fd[2];
	
    pid_t pids[5];
    
    int tmp = 0;
	int i, j, y, start = 0, end = 0;
    
    //Preenche com números aleatórios
    for(j = 0; j < 1000; j++) { 
        vec1[j] = rand()%10;
        vec2[j] = rand()%10;
    }
    
	if(pipe(fd) == -1){
		return 1;
	}
    
    for(i = 0; i < 5; i++) { 
        
        start = i * 200;
        end = (i + 1) * 200; 
        
        pids[i] = fork();

        if(pids[i] == 0) {
			 
			tmp = 0;
			
			for(y = start; y < end; y++) {
				tmp += vec1[y] + vec2[y];
			}
			
			printf("O ciclo começa em: %d | O ciclo acaba em: %d | Resultado da soma: %d\n", start, end, tmp);
			
			close (fd[0]);
			
			write(fd[1], &tmp, sizeof(int));
			
			if(i==4) {
				close(fd[1]);
			}
				
			exit(tmp);
			
        } else {
			wait(&status);
		}
    }
    
    close (fd[1]);
    int total = 0;
    for(i = 0; i < 5; i++) {
		read(fd[0], &tmp, sizeof(tmp));
			total += tmp;
	}
	
	printf("Total de todas as somas: %d\n", total);
    close (fd[0]);
    
	/*Enquanto o filho está a escrever, o write do pipe bloqueia porque o mesmo está cheio. 
	 *É isto que explica não precisar de um mecanismo de sincronização*/
	
return 0;
}
