#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

int main() {
    pid_t pid;
    srand(time(0));
    int fd[2][2], i, novaAposta, nrAleatorioFilho = 0, nrAleatorioPai = 0, credito = 20;
    
    for(i = 0; i < 2; i++){
        if (pipe(fd[i]) == -1) {
		    return -1;
	    }
    }
	
	pid = fork();
	
	srand(time(NULL) ^ getpid());
	
	if(pid > 0){
		close(fd[0][0]);
	    close(fd[1][1]);
	    printf("Crédito inicial: %d\n", credito);
	    do {

			nrAleatorioPai = rand()%5 + 1;
			
			if(credito > 0){
				novaAposta = 1;
			} else {
				novaAposta = 0;
			}
			if(write(fd[0][1], &novaAposta, sizeof(int)) == -1){
				perror("Erro de escrita!\n");
				exit(1);
			}
			
			if(novaAposta == 0){
				int status;
				wait(&status);
				if(!WIFEXITED(status)){
					perror("Erro no filho!\n");
					exit(1);
				}
				break;
			}
			
			if(read(fd[1][0], &nrAleatorioFilho, sizeof(int)) == -1){
				perror("Erro de leitura!\n");
				exit(1);
			}
			
			if(nrAleatorioFilho == nrAleatorioPai){
				credito += 10;
				printf("GANHOU!\n");
			} else {
				credito -= 5;
				printf("Perdeu!\n");
			}
			
			if(write(fd[0][1], &credito, sizeof(int)) == -1){
				perror("Erro de escrita!\n");
				exit(1);
			}
		} while(credito > 0);
		
		printf("\nO crédito disponível não lhe permite continuar a jogar!\n");
		printf("Fim do jogo!\n");
		
	    close(fd[0][1]);
	    close(fd[1][0]);
	    
	} else if (pid == 0){
	    close(fd[0][1]);
	    close(fd[1][0]);
	    
	    while(read(fd[0][0], &novaAposta, sizeof(int)) > 0){
			
			if(novaAposta == 0){
				exit(0);
			} else if(novaAposta == 1){
				
				do {
					printf("Insira um número entre 1 e 5.\n");
					scanf("%d", &nrAleatorioFilho);
				} while (nrAleatorioFilho > 5 || nrAleatorioFilho < 1);
				
				if(write(fd[1][1], &nrAleatorioFilho, sizeof(int)) == -1){
					perror("Erro de escrita!\n");
					exit(1);
				}
				
				if(read(fd[0][0], &credito, sizeof(int)) == -1){
					perror("Erro de leitura!\n");
					exit(1);
				}
				printf("Crédito disponível: %d\n\n", credito);
			}
		}
		
		close(fd[0][0]);
	    close(fd[1][1]);
	}
	
	int status;
	wait(&status);

    return 0;
}
