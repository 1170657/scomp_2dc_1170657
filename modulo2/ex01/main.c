#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

int main(void){
	
	int fd[2];
	pid_t filho;
	pid_t parent;
	int status; 
	
	/* cria o pipe */
	if(pipe(fd) == -1){
		return 1;
	}

	filho = fork(); 

	if(filho != 0) {
		printf("PID do Processo Pai: %d\n", getpid());
		parent = getpid();
		close(fd[0]);
		write(fd[1], &parent, sizeof(parent));
		close(fd[1]);
		wait(&status);
	} else {
            close(fd[1]);
			read(fd[0], &parent, sizeof(parent));
			close(fd[0]);
			printf("O Processo filho leu: %d\n", parent);
        }
	
return 0;

}
