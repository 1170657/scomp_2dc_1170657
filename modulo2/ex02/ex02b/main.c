#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
	int inteiro;
	char palavra[100];
} Struct;


int main(void){
	
	int fd[2];
	pid_t filho;
	
	char stringLida[100];
	int status;
	Struct s1;
	
	/* cria o pipe */
	if(pipe(fd) == -1){
		return 1;
	}

	filho = fork(); 

	if(filho != 0) {
		
		printf("Insira uma palavra ou construa uma frase:\n");
		fgets(stringLida, 100 , stdin);
		strcpy(s1.palavra,stringLida);
		
		printf("Insira um número inteiro:\n");
		scanf("%d", &s1.inteiro);
		
		close(fd[0]);
		
		write(fd[1], &s1, sizeof(Struct));
		
		close(fd[1]);
		wait(&status);
	} else {
            close(fd[1]);
            
			read(fd[0], &s1, sizeof(Struct));
			
			close(fd[0]);
			
			printf("\nO Processo filho leu: %s", s1.palavra);
			printf("O Processo filho leu: %d\n", s1.inteiro);
        }
return 0; 
}
