#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>

int main(void){
	int fd[2];
	pid_t filho;
	int inteiroLido;
	char stringLida[100];
	int status;
	
	/* cria o pipe */
	if(pipe(fd) == -1){
		return 1;
	}

	filho = fork(); 

	if(filho != 0) {
		
		printf("Insira uma palavra:\n");
		fgets(stringLida, 100 , stdin);
		
		printf("Insira um número inteiro:\n");
		scanf("%d", &inteiroLido);
		
		close(fd[0]);
		
		write(fd[1], &inteiroLido, sizeof(inteiroLido));
		write(fd[1], stringLida, sizeof(char)*100);
		
		close(fd[1]);
		wait(&status);
	} else {
            close(fd[1]);
            
			read(fd[0], &inteiroLido, sizeof(inteiroLido));
			read(fd[0], stringLida, sizeof(char)*100);
			
			close(fd[0]);
			
			printf("\nO Processo filho leu: %s", stringLida);
			printf("O Processo filho leu: %d\n", inteiroLido);
        }
	
return 0;
}
