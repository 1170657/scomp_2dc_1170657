#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>


int main() {
	
	int vec1[1000];
	int vec2[1000];
	int result[1000];
	
	int i, j,x, ret, status;
	
	time_t t;
	srand ((unsigned) time (&t));

	int fd[5][2];
	pid_t pid;
	
	for (x = 0; x < 1000; x++) {
		vec1[x] = rand()%200;
	}
	
	for (x = 0; x < 1000; x++) {
		vec2[x] = rand()%200;
	}
	
	for(i = 0; i<5; i++){
		if (pipe(fd[i]) == -1) {
			return -1;
		}
	}
	
	for (i = 0; i < 5; i++) {
		pid = fork();
		if (pid == -1) {
			exit(-1);
		}
		
		if (pid == 0) {
			close(fd[i][0]);
			int w = i*200;
			for(j = w; j< w+200; j++) {
				ret = vec1[j] + vec2[j];
				write(fd[i][1], &ret, sizeof(int));
			}							
			close(fd[i][1]);
			exit(0); 
		} else {
			wait(&status);
		}
	}
	for(i = 0; i < 5; i++) {
		close (fd[i][1]);
		int w = i*200;
			for(j = w; j< w+200; j++){
				read (fd[i][0], &ret, sizeof(int));
				result[j] = ret;
			}
		close (fd[i][0]);
	}
	
	for(i=0; i<1000; i++){
		printf("[%d] = %d\n", i, result[i]);
	}
	return 0;
}
