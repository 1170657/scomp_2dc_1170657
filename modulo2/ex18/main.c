#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Este programa tem como objetivo criar uma topologia de anél entre 5 processos como no exercício 12.
 * Cada processo escreve o seu pid e o pid do seu pai para que o seu filho respetivo leia.
 * De seguida fica à espera que o seu filho acabe para imprimir o seu pid.
 * O primeiro processo a ser criado é também o último a imprimir o seu pid visto que espera que o seu filho acabe.
 * Todos os filhos esperam que os seus respetivos filhos terminem, até que o último processo criado termine.
 */

int main(){
	int i, status, error, fd[2];
	pid_t pid;
	char message[100], read[100];
 
	pipe(fd);
	dup2(fd[0],0);	/* Reencaminha o stdin do processo actual para o descritor de leitura do pipe criado */
	dup2(fd[1],1);	/* Reencaminha o stdout do processo actual para o descritor de escrita do pipe criado */
	close(fd[0]);
	close(fd[1]);

	for(i=1;i<5;i++){
		pipe(fd);
		pid = fork();
		if(pid > 0)
			error = dup2(fd[1], 1);
		else
			error = dup2(fd[0], 0);

		close(fd[0]);
		close(fd[1]);

		if(pid)
			break;
	}

	sprintf(message,"This is process %d with PID %d and its %d\n", i, (int)getpid(), (int)getppid());
	write(1, message, strlen(message) + 1);
	read(0, read, strlen(message));
	wait(&status);
	fprintf(stderr, "Current process = %d, data =%s", (int)getpid(), read);
	exit(0);
}
