#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
 
int main() {

    int fd[2][2], i;
    
    pid_t pid; 
    
    for(i = 0; i < 2; i++){
		if (pipe(fd[i]) == -1) {
			perror("Erro no pipe!\n");
			exit(1);
		}
	}
 
    for (i = 0; i < 3; i++) {
        pid = fork();
 
        if (pid == 0) {
            break; 
        }
    }
    
    if (i == 0) {
		
        close(fd[1][1]);
        close(fd[1][0]);
        
        close(fd[0][0]);
        dup2(fd[0][1], 1); 
        close(fd[0][1]);   
        
        execlp("ls", "ls", "-la", (char*) NULL);
        exit(1);
    } 

    if (i == 1) {
		
        close(fd[0][1]);
        dup2(fd[0][0], 0); 
        close(fd[0][0]);
        
        close(fd[1][0]);
        dup2(fd[1][1], 1); 
        close(fd[1][1]);    
        
        execlp("sort", "sort", (char*) NULL);
        exit(1); 
    }

    if (i == 2) {
		
		close(fd[0][0]);
		close(fd[0][1]); 
		
        close(fd[1][1]);
        dup2(fd[1][0], 0); 
        close(fd[1][0]);
        
        execlp("wc", "wc", "-l", (char*) NULL);
        exit(1); 
    }
    
    close(fd[0][0]);
	close(fd[0][1]);
	close(fd[1][0]);
	close(fd[1][1]);
		     
    return 0;
}
