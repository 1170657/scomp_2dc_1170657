#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(){
	int numero;
	int factorial;
	
	read(0, &numero, sizeof(int));

	for(factorial = 1; numero > 1; numero = numero - 1){
		factorial = factorial * numero;
	}
	
	printf("%d\n", factorial);
	
	write(1, &factorial, sizeof(int));
	
	return 0;
}


