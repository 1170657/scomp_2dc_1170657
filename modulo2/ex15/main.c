#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
 
int main() {
    int fd[2][2], i, num = 0, factorial = 0;
    pid_t pid;
     
    for(i = 0; i < 2; i++){
		if (pipe(fd[i]) == -1) {
			perror("Erro no pipe!\n");
			exit(1);
		}
	}
    
    pid = fork();

    if (pid > 0) {
		
        while(num < 1) {
            printf("Insira um número: "); 
            scanf("%d", &num); 
        }
 
        close(fd[0][0]);
 
        if (write(fd[0][1], &num, sizeof(int)) == -1)  { 
            perror("Erro na escrita!\n");
            exit(1);
        }
      
        close(fd[0][1]);
        
        int status;
        wait(&status); 
        if (!WIFEXITED(status)) {
            perror("Erro no filho!\n");
            exit(1);
        }
        
        close(fd[1][1]);
        if(read(fd[1][0], &factorial, sizeof(int)) == -1){
			perror("Erro na leitura!\n");
            exit(1);
		}
		
	    close(fd[1][0]);

        printf("%d! = %d\n", num, factorial);  
        
        exit(0);  
 
    } else if(pid == 0){

        close(fd[0][1]);
        dup2(fd[0][0], 0);
        close(fd[0][0]);
 
        close(fd[1][0]);
        dup2(fd[1][1], 1);    
        close(fd[1][1]);
 
        execlp("./factorial", "./factorial", (char *) NULL);
        exit(1);
    }
 
    return 0;
}
