#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

int main(void){
	int fd[2], i, status;
	char message[1000] = "";
	pid_t pid;
	
	if (pipe(fd) == -1) {
		perror("Erro no pipe!\n");
		exit(1);
	}
	
	pid = fork();
	
	if(pid > 0){
		for(i = 0; i < 100; i++){
			sprintf(message, "%sLine %d\n", message, i+1);
		}
		
		close(fd[0]);
		if(write(fd[1], &message, sizeof(message)) == -1){
			perror("Erro de escrita!\n");
			exit(1);
		}
		close(fd[1]);
		
		wait(&status);
		if(!WIFEXITED(status)){
			perror("Erro no filho!\n");
		}
		
		
	} else if(pid == 0){
		close(fd[1]);
		dup2(fd[0], 0);
		close(fd[0]);
		
		execlp("more","more", (char *) NULL);
		exit(1);
	}
	
	return 0;
}
