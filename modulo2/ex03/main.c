#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(void){

	int fd[2];
	pid_t filho;
	int status; 
	
	char msg1[11] ="Olá Mundo!";
	char msg2[11] ="Adeus!";
	
	/* cria o pipe */
	if(pipe(fd) == -1){
		return 1;
	}

	filho = fork(); 

	if(filho != 0) {
		close(fd[0]);
		
		write(fd[1], msg1, sizeof(char)*10);
		write(fd[1], msg2, sizeof(char)*6);
		
		close(fd[1]);
		
		
	} else {
	
		close(fd[1]);
            
			read(fd[0], msg1, sizeof(char)*10);
			read(fd[0], msg2, sizeof(char)*6);
			
			close(fd[0]);
			
			printf("\nO Processo filho leu: %s\n", msg1);
			printf("O Processo filho leu: %s\n", msg2);
			
	}
	
	if(filho!=0){
		wait(&status);
		if (WIFEXITED(status)) { 
            int exit_status = WEXITSTATUS(status); 
            printf("\nValor de saída: %d", exit_status);
            printf("\nPID do Processo Pai: %d\n", getpid());
	}	
}
return 0; 
}
