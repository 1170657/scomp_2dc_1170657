#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

typedef struct {
   int customer_code;
   int product_code;
   int quantity;
} SALE; 

int main(void) {
    SALE sales[50000];
    srand(time(NULL));
	int i, status, start = 0, end = 0;
	int fd[2];
	pid_t pid[10];
	
    if (pipe(fd) == -1) {
		return 1;
	}
    
    /**
    *  Preenche o array de estruturas
    */
    
    for(i = 0; i < 50000; i++) { 
        sales[i].customer_code = i;
        sales[i].product_code = rand()%20000;
        sales[i].quantity = rand()%23;
    }
	
	for (i = 0; i < 10; i++) {
        start = i * 5000;
        end = (i + 1) * 5000; 
        
		pid[i] = fork();
		
		if (pid[i] == 0) {
            close(fd[0]);
            for(i = start; i < end; i++) {
                if(sales[i].quantity > 20) {
                    if(write(fd[1], &sales[i], sizeof(SALE)) == -1){
                        printf("Erro na escrita!! Processo: %d.\n", i);
                        exit(1);
                    }
                }
            }
            exit(0);
		}
	}
	
	for(i = 0; i < 10; i++) {
		wait(&status);
        if(WIFEXITED(status)){
            printf("O filho com o pid: %d saiu bem\n", pid[i]);
        } else {
            perror("Erro no filho!\n");
        }
	}
    
    /**
    *  O pai cria um apontador do tipo SALE com uma memória alocada dinamicamente
    *  O pai lê toda a informação do pipe e escreve no apontador.
    */
    close(fd[1]);
    int cont = 0;
    SALE *products = (SALE *) malloc(50000 * sizeof(SALE));
    
	while(read(fd[0], &products[cont], sizeof(SALE)) > 0) {
			cont++;
	}
	
	close(fd[0]);
    
    SALE *products_temp = (SALE *) realloc(products, (cont-1) * sizeof(SALE));
    products = products_temp;
    
    for(i = 0; i < cont; i++){
        printf("Produto: %d\n", products[i].product_code);
    }
	
    return 0;
}
