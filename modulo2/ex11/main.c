#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

int main(void) {
	int status, i, j, k, nrAleatorio = 0, nrAleatorioFilho = 0;
	pid_t pid;

	int fd[6][2];

	for(k = 0; k < 6; k++){
		if (pipe(fd[k]) == -1) {
			perror("Erro no pipe!\n");
			exit(1);
		}
	}
	
	for(i = 0; i < 5; i++){
		pid = fork();
		
		if(pid == 0){
			break;
		}
	}
	
	if(pid > 0){
		
		srand(time(NULL) ^ getpid());
		nrAleatorio = rand()%500 + 1;
		
		printf("PAI - PID: %d | Número gerado: %d\n", getpid(), nrAleatorio);
		
		close(fd[0][0]);
		if(write(fd[0][1], &nrAleatorio, sizeof(int)) == -1){
				perror("Erro de escrita!\n");
				exit(1);
		}
		close(fd[0][1]);
		
		for(j = 0; j < 5; j++){
			wait(&status);
			if(!WIFEXITED(status)){
				perror("Erro no filho!\n");
			}
		}
		
		close(fd[5][1]);
		if(read(fd[5][0], &nrAleatorio, sizeof(int)) == -1){
				perror("Erro de escrita!\n");
				exit(1);
		}
		close(fd[5][0]);
		
		printf("Maior número gerado: %d\n", nrAleatorio);
		
	} else if(pid == 0){
		
		srand(time(NULL) ^ getpid());
		nrAleatorioFilho = rand()%500 + 1;
		
		printf("FILHO - PID: %d | Número gerado: %d\n", getpid(), nrAleatorioFilho);
		
		close(fd[i][1]);
		if(read(fd[i][0], &nrAleatorio, sizeof(int)) == -1){
				perror("Erro de escrita!\n");
				exit(1);
		}
		close(fd[i][0]);
		
		if(nrAleatorioFilho > nrAleatorio){
			nrAleatorio = nrAleatorioFilho;
		}
		
		close(fd[i+1][0]);
		if(write(fd[i+1][1], &nrAleatorio, sizeof(int)) == -1){
				perror("Erro de escrita!\n");
				exit(1);
		}
		close(fd[i+1][1]);
	}
		
	return 0;
}
