#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>

int main(void) {

	int fd[2];
	pid_t filho;
	int status;
	int des;		// descritor do ficheiro
	int bytes; 		// tamanho do ficheiro em bytes
	
	char fileContent[100];	// buffer do pai
	char childContent[100];	// buffer do filho
	
	/* cria o pipe */
	if(pipe(fd) == -1){
		return 1;
	}

	filho = fork(); 

	if(filho > 0) {
		close(fd[0]);
		
		des = open("ex04.txt", O_RDONLY);
		bytes = read(des, fileContent, sizeof(fileContent));
		
		write(fd[1], fileContent, bytes);
		
		close(fd[1]);
		
		
	} else {
	
		close(fd[1]);
            
			read(fd[0], childContent, sizeof(childContent));
			
			close(fd[0]);
			
			printf("\nConteúdo do ficheiro: %s\n", childContent);
			
	}
	
	if(filho > 0){
		wait(&status);
		if (WIFEXITED(status)) { 
            return 0;
		}	
	}
	return 0;
}
