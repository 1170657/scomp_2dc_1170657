#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>

int main(void){
	
	int fd[2];
	int fd2[2];
	
	pid_t filho;
	
	char msgServer[100] = "";
	char msgClient[100] = "";
	
	int status;
	int i;
	
	/* cria o primeiro pipe */
	if(pipe(fd) == -1){
		return 1;
	}
	
	/* cria o segundo pipe */
	if(pipe(fd2) == -1){
		return 1;
	}

	filho = fork(); 

	if(filho == 0) {
		
		printf("Insira uma palavra ou frase:\n");
		fgets(msgClient, 100 , stdin);
		
		close(fd[0]);
		
		write(fd[1], msgClient, sizeof(char)*100);
		
		close(fd[1]);
		
		close(fd2[1]);
            
		read(fd2[0], msgServer, sizeof(char)*100);
		
		close(fd2[0]);
		
		printf("Palavra ou frase alterada: %s\n", msgServer);
		
	} else {
		
            close(fd[1]);
            
			read(fd[0], msgClient, sizeof(char)*100);
			
			for(i = 0; i <= strlen(msgClient); i++){
				if(msgClient[i] >= 97 && msgClient[i] <= 122){
					msgServer[i] = msgClient[i] - 32;
				} else if (msgClient[i] == 32) {
					//se for espaço
					msgServer[i] = msgClient[i];
				}
			}
			
			close(fd[0]);
			
			close(fd2[0]);
			
			write(fd2[1], msgServer, sizeof(char)*100);
			
			close(fd2[1]);
			
			wait(&status);
	}     
	
	return 0;
}
