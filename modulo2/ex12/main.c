#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

typedef struct {
   int codigo_barras;
   int preco;
   char *nome;
} PRODUTO; 

int main(void){
	PRODUTO produtos[5];
	PRODUTO prod;
	int fd[6][2], cod_barras = 0, i, j, status;
	pid_t pid;
	
	for(i = 0; i < 6; i++){
		if (pipe(fd[i]) == -1) {
			perror("Erro no pipe!\n");
			exit(1);
		}
	}
	
	for(i = 0; i < 5; i++){
		pid = fork();
		
		if(pid == 0){
			cod_barras = i + 1;
			
			if(write(fd[5][1], &cod_barras, sizeof(int)) == -1){
				perror("Erro de escrita!\n");
				exit(1);
			}
			
			close(fd[i][1]);
			if(read(fd[i][0], &prod, sizeof(PRODUTO)) == -1){
				perror("Erro de leitura!\n");
				exit(1);
			}
			close(fd[i][0]);
			
			printf("Código de barras: %d | Nome: %s | Preço: %d\n\n", prod.codigo_barras, prod.nome, prod.preco);
			exit(0);
		}
	}
	
	if(pid > 0){
		for(i = 0; i < 5; i++){
			produtos[i].codigo_barras = i + 1;
			produtos[i].preco = i + 10;
		}
		
		produtos[0].nome = "Camisola";
		produtos[1].nome = "Camisa";
		produtos[2].nome = "Calças";
		produtos[3].nome = "Brincos";
		produtos[4].nome = "Pulseira";
		
		close(fd[5][1]);
		
		for(i = 0; i < 5; i++){
			if(read(fd[5][0], &cod_barras, sizeof(int)) == -1){
				perror("Erro de leitura!\n");
				exit(1);
			}
			
			for(j = 0; j < 5; j++){
				if(cod_barras == produtos[j].codigo_barras){
					prod = produtos[j];
				}
			}
			
			close(fd[i][0]);
			if(write(fd[i][1], &prod, sizeof(PRODUTO)) == -1){
				perror("Erro de escrita!\n");
				exit(1);
			}
			close(fd[i][1]);
		}
		close(fd[5][1]);
	}
	
	for(j = 0; j < 5; j++){
		wait(&status);
		if(!WIFEXITED(status)){
			perror("Erro no filho!\n");
		}
	}
	
	return 0;
}
