#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

struct Jogo {
   char  mensagem[10];
   int   nrRodada;
}; 

int main(void) {
    struct Jogo jogo;
	int i, status;
	int fd[2];
	pid_t pid[10];
	
	if (pipe(fd) == -1) {
		return -1;
	}
	
	for (i = 0; i < 10; i++) {
		
		pid[i] = fork();
		
		if (pid[i] == 0) {
		    close(fd[1]);
            read (fd[0], &jogo, sizeof(jogo));
            printf("Eu ganhei! Mensagem: %s | Nº da ronda: %d\n", jogo.mensagem, jogo.nrRodada);
            exit(jogo.nrRodada);
		}
	}
	close(fd[0]);
	
	for(i = 1; i < 11; i++) {
		sleep(2);
        strcpy(jogo.mensagem, "Win!");
        jogo.nrRodada = i;
        write(fd[1], &jogo, sizeof(jogo));
	}
	
	printf("\n");
	
	close(fd[1]);
	for(i = 0; i < 10; i++) {
		wait(&status);
        if(WIFEXITED(status)){
            int exit_status = WEXITSTATUS(status);
            printf("O filho com o PID: %d venceu a ronda nº %d\n", pid[i], exit_status);
        }
	}
	
    return 0;
}
