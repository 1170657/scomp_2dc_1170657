#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(){
	pid_t pid;
	char password[50];
	int fd[2], id, status;
	
	if (pipe(fd) == -1) {
		perror("Erro no pipe!\n");
		return 1;
	}
	
	pid = fork();
	
	if (pid == -1) {
		perror("Erro no fork!\n");
		exit(1);
	}
	
	if (pid > 0 ) {
		printf("Insira o seu ID: ");
		scanf("%d", &id); 
		
		printf("Insira a sua password: ");
		scanf("%s", password);
		
		close(fd[0]);
		
		write(fd[1], &id, sizeof(int));
		write(fd[1], password, sizeof(password));
		
		close (fd[1]);
		
		wait (&status);
		if(WIFEXITED(status)){
			switch(WEXITSTATUS(status)) {
				case 0: 
					printf("Password verified\n\n");
					break;
				case 1:
					printf("Invalid password\n\n");
					break;
				case 2:
					printf("No such user\n\n");
					break;
				default:
					perror("Erro");
					exit(1);
			}
		} else if(!WIFEXITED(status)){
			perror("Erro no filho!\n");
		}
	} else {
		close (fd[1]);
		dup2(fd[0], 0);
		close (fd[0]);
		
		execlp ("./validate", "validate", (char*) NULL);
		perror ("Exec falhou");
		exit(0);
		
	}
			
	return 0;
}
