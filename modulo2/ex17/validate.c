#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main()
{
	char password[50] = "\0";
	int id = 2;
	char *c = "scomp";
	
	read(0, &id, sizeof(int));
	read(0, password, sizeof(password));
	
	if (id != 2) {
		exit(2);
	}
	
	if (strcmp(password, c) != 0) {
		exit(1);
	}
	
	exit(0);
}

