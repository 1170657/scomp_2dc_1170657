#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "word_occurrences.h"

typedef struct {
	char path[100];
	char word[50];
	int nr_ocurrences;
} child_info;

typedef struct {
	child_info child_info1;
	int permissao1;
} permissao;

int main () {
	pid_t p;
	
	int fd = shm_open("/shm_files", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	ftruncate(fd, sizeof(permissao)*10);
	
	permissao * data = (permissao *) mmap (NULL, sizeof(int)*10, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	int i=0; 
     
	data->permissao1 = 0;
	
    for (i = 0; i < 10; i++) {
       
        printf("File name %d? ", i+1);
        scanf("%s", data[i].child_info1.path);
       
        printf("Word %d? ", i+1);
        scanf("%s", data[i].child_info1.word);
 
        data[i].child_info1.nr_ocurrences = 0;
    }
    
    data->permissao1 = 1;
    
    for(i = 0; i < 10; i++){
		p = fork();
		if(p==0){
			break;
		} else if (p==-1){
			perror("Erro no fork.\n");
		}
	}
	
	if(p>0){
		
		int status;
		for(i = 0; i<10; i++){
			wait(&status);
			if(!WIFEXITED(status)){
				exit(-1);
			}
		}
		
		for(i = 0; i<10; i++){
			printf("File %s, Word %s, Ocurrences %d.\n", data[i].child_info1.path, data[i].child_info1.word, data[i].child_info1.nr_ocurrences);
		}

		munmap(data, sizeof(child_info)*10);
		
		shm_unlink("/shm_files");
		
	} else {
		
		while(data->permissao1 == 0);
		
		data[i].child_info1.nr_ocurrences = word_occurrences(data[i].child_info1.path, data[i].child_info1.word);
		
		munmap(data, sizeof(child_info)*10);
		
		exit(0);
		
	}
	
	return 0;
}
