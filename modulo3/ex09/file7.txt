Se na tua dura vida a felicidade
Parece estar em distante lugar,
Maior precisa ser a tua vontade
De persegui-la até a encontrar.

E nunca temas uma tempestade,
Não deixes nada te desanimar,
Quanto maior for a dificuldade,
Com mais empenho deves lutar.

Sigas em frente com humildade,
Buscando o próximo respeitar,
Propagues a justiça e a verdade
E assim farás o mundo melhorar.

Aos princípios tenhas fidelidade,
O amor e a fé podem tudo mudar,
O teu sonho se tornará realidade
Se nele não deixares de acreditar.
