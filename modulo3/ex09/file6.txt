Nunca desista das pessoas que:
Secam sua lágrimas;
Te fazem sorrir;
Te abraçam apertado e te confortam;
Demostram que te ama em um olhar;
Cuidam de você;
Ajudam no que precisar;
Mudam por você;
Sorriem quando te vêem;
Colocam você nos planos para o fim de semana;
Mesmo não fazendo nada, indo a lugar algum, ainda assim mudam seu dia;
Te magoam, justamente por te amar e por fim, jamais, 
desista das pessoas que você ama e que amam você, pois, só quem te ama,
faz tudo isso aí em cima e muito muito mais para te fazer feliz!
