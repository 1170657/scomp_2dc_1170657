#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

int word_occurrences(char * path, char * word){
	
	FILE *file;
	int cont = 0;
	int ch;
	
	file = fopen(path, "r"); //abrir o ficheiro em modo leitura
	
	if (file == NULL){
		return -1;
	}
	
	int len = strlen(word);
	
	while(1) {
		
		int i;
		
		ch = fgetc(file); //char a char
		if(ch == EOF){
			fclose(file);
			return cont;
		}
		
		if((char) ch != *word){ //0 fica preenchido
			continue;//Volta ao while
		}
		
		for(i = 1; i < len; i++){
			
			ch = fgetc(file);
			if(ch == EOF){
				fclose(file);
				return cont;
			}
			
			if(ch != word[i]){
				//SEEK_CUR move o apontador do ficheiro para a posição dada no segundo parametro
				fseek(file, 1-i, SEEK_CUR); 
				break;// sai do for, vai while
			}
			
			if(i == len - 1 && ch == word[i]){
				cont++;
			}
			
		}
		
	}
	
	return cont;
}
