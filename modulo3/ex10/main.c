#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
 
#define TAMANHO_BUFFER 10	

typedef struct {
    int valores[TAMANHO_BUFFER];	
    int head;	
    int tail;	
} info;

typedef struct {
    info info1;
    int permissao1;	
} permissao;
 
int main() {
    
    int fd = shm_open("/shm_ex10", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    
    if(ftruncate(fd, sizeof(permissao)) == -1){
        perror("Erro!\n");
    }

    permissao * shared = (permissao *) mmap(NULL, sizeof(info), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	shared->info1.head = 0; 
    shared->info1.tail = 0;
    shared->permissao1 = 0;
    
    int i, j, m;
    
    i = 0;
    
    pid_t p;
    p = fork();
    
    //Processo produtor -> incrementa a cabeça
    if (p > 0) {
		
		for (i = 0; i < 10; i++) {
        shared->info1.valores[i] = 0;
		}

		i = 0;

		for(j = 0; j < 3; j++){
			
			//Quando a tail e a head são iguais, não executa o while
			while(shared->permissao1 == 0){ 
				shared->info1.valores[shared->info1.head] = i;
				shared->info1.head = ((shared->info1.head + 1) % TAMANHO_BUFFER);
				i++;
				if(shared->info1.head == 0){
					shared->permissao1 = 1;
				}
			}
			
			while(shared->permissao1 == 1 && j < 2);
		}
		
        int status;
        p = wait(&status);
        if (!WIFEXITED(status)) {
            exit(-1);
        }
 
        munmap(shared, sizeof(permissao));
 
		shm_unlink("/shm_ex10");
		
	  //processo consumidor -> incrementa a cauda
    } else if(p == 0){

        for(m = 0; m < 3; m++) {
			
			while(shared->permissao1 == 1){
				
				printf("Inteiro número: %d || TAIL: %d\n", shared->info1.valores[shared->info1.tail%TAMANHO_BUFFER], shared->info1.tail);
				
				shared->info1.tail = ((shared->info1.tail + 1) % TAMANHO_BUFFER);
				i++;
				
				if(shared->info1.tail == 0){//verifica que o número que é partilhado é sempre pelo menos +1 que o anterior. 
					printf("BUFFER CHEIO! Inteiro número: %d || TAIL: %d\n", shared->info1.valores[shared->info1.tail%TAMANHO_BUFFER], shared->info1.tail);
					shared->permissao1 = 0;
				}
			}
			
			while(shared->permissao1 == 0 && m < 2);
		}
		
        munmap(shared, sizeof(permissao));
    }  
     
    return 0;
}  
     
   

