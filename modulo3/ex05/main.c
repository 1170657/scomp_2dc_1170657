#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

int main(void) {

		int vec[1000];
		int maximos[10];
		int * v;
		v = maximos;
		
		time_t t;
		
		srand ((unsigned) time(&t));
		
		int x;
		for (x = 0; x < 1000; x++) {
			vec[x] = (rand()%1000)+1;
		}

		int fd = shm_open("/shm_matriz", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    
		if(fd == -1){
			exit(-1);
		}
     
		ftruncate(fd, sizeof(int)*10);
 
		v = mmap(NULL, sizeof(int)*10, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  
		munmap(maximos, sizeof(int)*10);
		
		int maximo;
		int i = 0, j = 0, k = 0, start = 0, end = 0;
		
		for(j=0; j < 10; j++) {
			
			pid_t filho = fork();
			
			if(filho == 0) {
				start = i * 100;
				end = (i + 1) * 100; 
			
				for(i=start; i < end; i++) {
					if (vec[i+1]>vec[i]) {
						maximo = vec[i+1];
					}
				}
				
				maximos[k] = maximo;
				exit(1);
			 }
			
				k++;
			}

		int w, status;
			for(w=0; w<11; w++) {
				wait(&status);
				if(!WIFEXITED(status)) {
					exit(-1);
				}
			}
		
		int y = 0, maiorMaximo = 0;
		for(y = 0; y < 10; y++) {
			
			if(maximos[y+1] > maximos[y]) {
				maiorMaximo = maximos[y+1];
				
			}
			
			printf("O maior número do filho %d é %d\n", y, maiorMaximo);
		}
		
		munmap(v, sizeof(int)*10); //inverso de mmap -> Disconecta zona de memória armazenada
    
		close(fd);

		shm_unlink("/shm_matriz"); //marca para ser apagado assim que todos os processos que o usem terminem 
    
		//remove -> munmap(), close(), shm_unlink()
    
    return 0;

}
