#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>

int main() {

    int fd = shm_open("/shm_number", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	if(fd == -1){
		fprintf(stderr, "Erro shm_open\n");
		exit(1);
	}
     
    ftruncate(fd, sizeof(int));
 
    int * number = (int *) mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    *number = 100;   
 
    pid_t p;
    p = fork();
    
    int i;
    for (i = 0; i < 1000000; i++) {
        number++;
        number--;
        printf("%s Valor: %d\n", (p == 0) ? "Filho" : "Pai", *number);
    }
 
    munmap(number, sizeof(int));

    if (p > 0) {
        int status;
        wait(&status);
        if(!WIFEXITED(status)) {
            fprintf(stderr, "Erro no filho.\n");
            exit(1);
        }
    }
     
    shm_unlink("/shm_number");
     
    return 0;
}
