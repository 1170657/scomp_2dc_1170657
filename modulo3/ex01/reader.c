#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

typedef struct {
	char nome[50];
    int numero;
} estudante;
 
int main() {
 
    int fd = shm_open("/shm_estudante", O_RDWR, S_IRUSR | S_IWUSR); //abre a memória partilhada
    
    if(fd == -1){
		exit(-1);
	}
	
    ftruncate(fd, sizeof(estudante)); //define o tamanho
 
    estudante * estudante_ap = (estudante *) mmap(NULL, sizeof(estudante), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0); //cria ligação 

	//create -> shm_open(), ftruncate(), mmap() 
	
    fprintf(stdout, "\nESTUDANTE:\n-> Nome: %s\n-> Número: %d\n", estudante_ap->nome, estudante_ap->numero);
    
    munmap(estudante_ap, sizeof(estudante)); //inverso de mmap -> Disconecta zona de memória armazenada
    
    close(fd);

    shm_unlink("/shm_estudante"); //marca para ser apagado assim que todos os processos que o usem terminem 
    
    //remove -> munmap(), close(), shm_unlink()

    return 0;
}
