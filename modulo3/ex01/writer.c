#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

typedef struct {
	char nome[50];
    int numero;
} estudante;
 
int main() {
 
    int fd = shm_open("/shm_estudante", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR); //abre a memória partilhada
    
    if(fd == -1){
		exit(-1);
	}
	
    ftruncate(fd, sizeof(estudante)); //define o tamanho
     
    estudante * estudante_ap = (estudante*) mmap(NULL, sizeof(estudante), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0); //cria ligação 

    fprintf(stdout, "Introduza o número do Estudante: ");
    scanf("%d", &estudante_ap->numero);    
     
    fprintf(stdout, "Introduza o nome do Estudante: ");
    scanf("%s", estudante_ap->nome);
     
    munmap(estudante_ap, sizeof(estudante)); //inverso de mmap -> Disconecta zona de memória armazenada
    
    return 0;
}
