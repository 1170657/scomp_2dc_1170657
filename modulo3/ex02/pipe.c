#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>


#define ARRAY_SIZE 1000000


int main()
{
	clock_t begin = clock();
	pid_t pid;
	int i, fd[2];
	int transfer[1000000];
	
	time_t t;
	srand ((unsigned) time (&t));
	
	for (i = 0; i < 1000000; i++){
		transfer[i] = rand()%100;
	}
	
	
	if (pipe(fd) == -1) {
		perror("Pipe falhou");
		return 1;
	}
	
	pid = fork();
	if ( pid == -1) {
		perror("Fork falhou");
		exit(1);
	}
	
	if (pid == 0) {
		close (fd[0]);
		for (i = 0; i < 1000000; i++){
			write(fd[1], &transfer[i], sizeof(transfer)/sizeof(int));
		}		
		close(fd[1]);
		exit(0);
	}
	
	close (fd[1]);
	int cont = 0;
	while(read(fd[0], &transfer[cont], sizeof(transfer)/sizeof(int)) > 0){
		printf("Numero %d: %d\n", cont, transfer[cont]);
		cont++;
	}	
	close (fd[0]);
	
	int status;
	wait(&status);
    if(!WIFEXITED(status)){
		perror("Erro no filho!\n");
	}
	
	clock_t end = clock();
	printf("Tempo: %f\n", (double)(end-begin)/CLOCKS_PER_SEC);
	return 0;
}

