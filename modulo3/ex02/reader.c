#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#define ARRAY_SIZE 1000000


int main()
{
	
	int i, fd, data_size = ARRAY_SIZE * sizeof(int);
	int numerosRandom[ARRAY_SIZE];
	int *shared_data;
	
	fd = shm_open("/memoriaPartilhada", O_EXCL | O_RDWR,  S_IRUSR|S_IWUSR);
	if (fd == -1) {
		perror("Erro a abrir a shared memory");
		return 0;
	}
	if(ftruncate(fd, data_size) == -1) {
		perror("Erro");
		exit(1);
	} 
	shared_data = (int*)mmap(NULL, ARRAY_SIZE * sizeof(int),PROT_READ|PROT_WRITE,MAP_SHARED, fd, 0);
	if(shared_data == NULL) {
		perror("Erro");
		exit(0);
	}
	
	for (i = 0; i < ARRAY_SIZE; i++)
	{
		*(shared_data + i) = *(numerosRandom + i);
	}
	munmap (shared_data, ARRAY_SIZE * sizeof(int));
	close (fd);
	
	char string [2];
	printf("Deseja apagar a memória partilhada? [S/N]\n");
	fgets(string, sizeof(string),stdin);
	if (string[0] == 'S') {
		shm_unlink("/memoriaPartilhada");
	}
	return 0;
}

