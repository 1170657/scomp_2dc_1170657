#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>

#define STR_SIZE 50
#define NR_DISC 10
 
typedef struct {
    int numero;
    char nome[STR_SIZE];
    int disciplinas[NR_DISC];
} aluno;

typedef struct {
	aluno aluno1;
	int permissao1;
}permissao;
 
int main(){
 
    int fd = shm_open("/shm_aluno", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

    ftruncate(fd, sizeof(permissao));

    permissao * data = (permissao *) mmap(NULL, sizeof(permissao), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    
    pid_t p;
    p = fork();
 
    if (p > 0) {
		
	data->permissao1 = 0;
		
	printf("Nome? ");
    scanf("%s", data->aluno1.nome);
    
	do {
        printf("Número? ");
        scanf("%d", &data->aluno1.numero);
    } while(data->aluno1.numero <= 0);

    int i;
    
    for (i = 0; i < NR_DISC; i++) {
        do {
            printf("Nota da disciplina %d? ", i+1);
            scanf("%d", &data->aluno1.disciplinas[i]);
        } while(data->aluno1.disciplinas[i] < 0);       
    }
    
    data->permissao1 = 1;
	
    int status;
    wait(&status);
    if (!WIFEXITED(status)) {
		exit(-1);
    }
         
     munmap(data, sizeof(permissao));
 
     shm_unlink("/shm_aluno");
	 
    } else {
		
		while(data->permissao1 == 0);
		
        printf("\nAluno\n Nome: %s\n Número: %d\n", data->aluno1.nome, data->aluno1.numero);
         
        int max = data->aluno1.disciplinas[0];
        int min = data->aluno1.disciplinas[0];
        
        int sum = 0, i;
        
        for (i = 0; i < NR_DISC; i++){
			
            sum += data->aluno1.disciplinas[i];
 
            if (data->aluno1.disciplinas[i] > max) {
                max = data->aluno1.disciplinas[i];
            }
 
            if (data->aluno1.disciplinas[i] < min) {
                min = data->aluno1.disciplinas[i];
            }
         }
            
        double avg = (double) sum/NR_DISC;
        
        printf("\nMaior nota: %d\nMenor nota: %d\nMédia de notas: %.2f\n", max, min, avg);
         
        munmap(data, sizeof(permissao));
	}
    
    return 0;
}
