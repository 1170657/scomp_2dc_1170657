#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

int main ()
{
	
	int fd = shm_open("/shm_vector", O_RDWR, S_IRUSR | S_IWUSR);
	
	ftruncate (fd, sizeof(int)*10);
	
	int * vec = (int *) mmap(NULL, sizeof(int)*10, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	int soma = 0, i;
    for (i = 0; i < 10; ++i)
    {
        soma += vec[i];
    }
	
	printf("A média dos 10 valores do vetor é: %.2f\n", (double)soma/10);
	
	munmap(vec, sizeof(int)*10);
	
	shm_unlink("/shm_vector");
	
	return 0;
}
