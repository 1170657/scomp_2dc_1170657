#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

int main()
{
	
	int v[10];
	int * vec = v;
	int i;
	
	int fd = shm_open("/shm_vector", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	ftruncate (fd, sizeof(int)*10);
	
	vec = (int *) mmap(NULL, sizeof(int)*10, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	srand ((unsigned) time(NULL));
	
	for(i = 0; i<10; i++){
		vec[i] = (rand() % 20) + 1;
	}
	
	munmap(vec, sizeof(int)*10);
	
	return 0;
}
