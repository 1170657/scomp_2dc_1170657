#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
 
#define TAMANHO_BUFFER 10	

typedef struct {
    int valores[TAMANHO_BUFFER];	
    int head;	
    int tail;	
} info;
 
int main() {
	
	int fd[2];
	int fd2[2];
	info shared;
	
	int i, j, m;
    
    for (i = 0; i < 10; i++) {
        shared.valores[i] = 0;
    }

	//Sempre que um novo número é escrito no array circular, a cabeça
	//(head) do array é incrementada.
	//Sempre que um novo número é lido, a cauda é incrementada. 
	shared.head = 0; 
    shared.tail = 0;
    
    i = 0;
    
    if(pipe(fd) == -1){
		perror("Erro no pipe!\n");
		exit(-1);
	}
	
	if(pipe(fd2) == -1){
		perror("Erro no pipe!\n");
		exit(-1);
	}
    
    pid_t p;
    p = fork();
    
    //Processo produtor -> incrementa a cabeça
    if (p > 0) {

		for(j = 0; j < 3; j++){
			
			while(((shared.head) + 1) % TAMANHO_BUFFER != (shared.tail) % TAMANHO_BUFFER){
				
				shared.valores[shared.head] = i;
				
				if(write(fd[1], &shared.valores[shared.head], sizeof(int)) == -1) {
					perror("Erro na escrita do pai!\n");
	     	
	     	
	     			exit(-1);
				}
				
				shared.head = ((shared.head + 1) % TAMANHO_BUFFER);
				
				if(shared.head == 0){
					break;
				}
				
				i++;
				
				if(read(fd2[0], &shared.tail, sizeof(int)) == -1) { //lê o valor da cauda que o filho manda
					perror("Erro na leitura do pai!\n");
					exit(-1);
				}
				
			}
		}
		
        int status;
        p = wait(&status);
        if (!WIFEXITED(status)) {
            fprintf(stderr, "Erro no filho.\n");
            exit(1);
        }
		
	  //processo consumidor -> incrementa a cauda
    } else if(p == 0){
		
        for(m = 0; m < 3; m++) {
			
			while(1){
				
				if(read(fd[0], &shared.valores[shared.tail], sizeof(int)) == -1) {
					perror("Erro na leitura do filho!\n");
					exit(-1);
				}
				
				if(shared.tail%TAMANHO_BUFFER == TAMANHO_BUFFER - 1){ 
					printf("BUFFER CHEIO! Inteiro número: %d || TAIL: %d\n", shared.valores[shared.tail%TAMANHO_BUFFER], shared.tail);
					shared.tail = ((shared.tail + 1) % TAMANHO_BUFFER);
					break;
				}
				
				printf("Inteiro número: %d || TAIL: %d\n", shared.valores[shared.tail%TAMANHO_BUFFER], shared.tail);
				
				shared.tail = ((shared.tail + 1) % TAMANHO_BUFFER);
				
				if(write(fd2[1], &shared.tail, sizeof(int)) == -1) {
					perror("Erro na escrita do filho!\n");
					exit(-1);
				}
			}
		}
		
		exit(0);
    }   
    return 0;
}
+
