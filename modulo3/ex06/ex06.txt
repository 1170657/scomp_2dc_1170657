A melhor performance estaria diretamente relacionada com a execução de apenas 1 processo. 
Tendo em conta a arquitectura do meu computador, que só possuí unicamente 1 core, só tem capacidade de processamento de um processo, de cada vez.
Isto implica que, se houver mais do que um processo, os restantes têm que esperar pelo término do atual.
O tempo de espera entre processos é superior ao tempo de execução da função enquanto esta só apresenta apenas 1 processo. 
Em suma, é mais rápido e eficiente, a execução de um só processo. 
