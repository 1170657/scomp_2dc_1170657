#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main() {
    pid_t pid;
    
    time_t t;
	srand ((unsigned) time (&t));
	
	int vec[1000];
    int y, x, i;
    int start = 0, end = 0;
    int maximoLocal = 0, maximoGlobal = 0, filhoComMaiorMaximo = 0;
    
    int fd[2]; 
    
    if (pipe(fd) == -1) {
		return -1;
    }
    
    for (x = 0; x < 1000; x++) {
		vec[x] = rand()%1000;
	}
	
	for(i = 0; i < 10; i++) {
		
		start = i * 100;
        end = (i + 1) * 100; 
		
		pid = fork();
		if (pid == -1) {
			exit(-1);
		} else if (pid == 0) {
			
			close(fd[0]);
			
			for(y = start; y < end; y++) {
				if (vec[y]>maximoLocal) {
					maximoLocal = vec[y];
				}
			}	
				
            if(write(fd[1], &maximoLocal, sizeof(int)) == -1){
                perror("Erro de escrita!\n");
				exit(-1);
            }
            
			exit(i); 
		}
	}
	
	if(pid > 0){
		
        int status = 0, exit_status = 0;
        
		close(fd[1]);
		for(i = 0; i < 10; i++){
			
			wait(&status);
			
			if(WIFEXITED(status)){
				
               if(read(fd[0], &maximoLocal, sizeof(int)) == -1){
					perror("Erro de leitura!\n");
					exit(1);
			    } else {
					exit_status = WEXITSTATUS(status);	
				}
            
				if(maximoLocal > maximoGlobal){
					maximoGlobal = maximoLocal;
					filhoComMaiorMaximo = exit_status;
				}  
          }
			
		}
		
        printf("Máximo Global: %d | Filho nº: %d\n", maximoGlobal, filhoComMaiorMaximo);
        close(fd[0]);
		
	}
	
    return 0;
}
