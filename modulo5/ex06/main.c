#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <limits.h>

int numero;

//---- Produz todos os nrs primos que são menores ao iguais ao numero digitado ----
void* thread_funcao(void* arg){
	int bool, i, j;

	printf("\nNúmeros primos abaixo ou iguais a %d:\n", numero);
	for (i = 2; i < numero; i++){
		bool = 1;

		for (j = 2; j <= i/2; j++){
			if (i%j == 0){
				bool = 0;
				break;
			}
		}

		if (bool == 1){
			printf("%d\n", i);
		}
	}
	
	pthread_exit(NULL);
}
//-------------------------------------------------------------------------------

int main(){
	
	pthread_t thread;
	
	printf("Insira um número:");
	scanf("%d", &numero);
	
	//-------- Criação da Thread ---------------------
	pthread_create(&thread, NULL, thread_funcao, NULL);
	//------------------------------------------------
	
	//-------- Espera que a Thread acabe -------------
	pthread_join(thread, NULL);
	//------------------------------------------------
	
	return 0;
}
