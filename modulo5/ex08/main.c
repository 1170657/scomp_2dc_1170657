#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <limits.h>

int dados[1000];
int resultados[1000];

//------------- Realiza o cálculos para preencher o vetor resultados --------------
void* thread_funcao(void *arg){
	int i;
	int start = (int) arg;
	for (i = start * 200; i < (start * 200) + 200; i++)	{
		resultados[i] = dados[i] * 2 + 10;
	}

	pthread_exit(NULL);
}
//--------------------------------------------------------------------------------

int main(){
	pthread_t threads[5];
	
	int i;
	
	//-------- Preenche o Array--------------
	srand(time(NULL));
	for (i = 0; i < 1000; i++){
		dados[i] = rand() % 50;
	}
	//--------------------------------------
	
	//------------------ Criação das 5 Threads ------------------------
	for (i = 0; i < 5; i++){
		pthread_create(&threads[i], NULL, thread_funcao, (void*)i);
	}	
	//-----------------------------------------------------------------
	
	//----------------- Espera que acabem -----------------------------
	for (i = 0; i < 5; i++){
		pthread_join(threads[i], NULL);
	}
	//-----------------------------------------------------------------
	
	for (i = 0; i < 1000; i++){
		printf("Sou o dado %d e o meu resultado é %d\n", i+1, resultados[i]);
	}
	
	return 0;
}
