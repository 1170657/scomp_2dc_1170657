#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>

int matriz1[5][5];
int matriz2[5][5];
int resultado[5][5];

//--------------- Preenche a MATRIZ1 e dá print dela -------------------
void* thread_funcao_MATRIZ1(void *arg){
	srand(time(NULL));
	int i, j;
	for (i = 0; i < 5; i++){
		for (j = 0; j < 5; j++){
			matriz1[i][j] = ((rand()%10));
		}
		
	}
	
	printf("\nMATRIZ 1\n");
	for (i = 0; i < 5; i++){
		printf("| ");
		for (j = 0; j < 5; j++){
			printf("%d | ", matriz1[i][j]);
		}
		printf("\n");
	}
	
	pthread_exit((void*) NULL);	
}
//----------------------------------------------------------------------

//--------------- Preenche a MATRIZ2 e dá print dela -------------------
void* thread_funcao_MATRIZ2(void *arg){
	srand(time(NULL));
	int i, j;
	for (i = 0; i < 5; i++){
		for (j = 0; j < 5; j++){
			matriz2[i][j] = ((rand()%10));
		}
		
	}
	
	printf("\nMATRIZ 2\n");
	for (i = 0; i < 5; i++){
		printf("| ");
		for (j = 0; j < 5; j++){
			printf("%d | ", matriz2[i][j]);
		}
		printf("\n");
	}
	
	pthread_exit((void*) NULL);	
}
//----------------------------------------------------------------------

//------------------------ MULTIPLICA ----------------------------------
void* thread_funcao_MULTIPLICA(void *arg){
	int value = *((int *) arg);
	int j;
	for (j = 0; j < 5; j++){
		resultado[value][j] =  matriz1[value][j] * matriz2[value][j];
	}
	
	
	pthread_exit((void*) NULL);	
}
//----------------------------------------------------------------------


int main(){
	
	pthread_t thread1;
	pthread_t thread2;
	pthread_t threads_multiplicacao[5];
	
	int i,j;

	//-------- Cria as 2 Threads necessárias para preencher as matrizes ------------
	pthread_create(&thread1, NULL, thread_funcao_MATRIZ1,NULL);
	printf("\nCriou a Thread para preencher a MATRIZ1\n");
	pthread_join(thread1, NULL);
	
	pthread_create(&thread2, NULL, thread_funcao_MATRIZ2,NULL);
	printf("\nCriou a Thread para preencher a MATRIZ2\n");
	pthread_join(thread2, NULL);
	//-----------------------------------------------------------------------------

	//--------- Cria as 5 Threads necessárias para multiplicar as 2 matrizes ----------------
	int args[5];
	printf("\n");
	for (i = 0; i < 5; i++){
		args[i] = i;
		pthread_create(&threads_multiplicacao[i], NULL, thread_funcao_MULTIPLICA,(void*)&args[i]);
		printf("Criou a thread para multiplicar a linha %d\n", args[i]+1);
	}
	//---------------------------------------------------------------------------------------
	
	//------------------ Espera por todas as Threads e Imprime -------------------------------
	for (i = 0; i < 3; i++){
		pthread_join(threads_multiplicacao[i], NULL);
	}
	
	//A Tread principal imprime o resultado final
	printf("\nMATRIZ RESULTADO\n");
	for (i = 0; i < 5; i++){
		printf("| ");
		for (j = 0; j < 5; j++){
			printf("%d | ", resultado[i][j]);
		}
		printf("\n");
	}
	//-------------------------------------------------------------------------------------
	
	
	return 0;
}
