#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>

typedef struct {
	int numeroThread;
	int posicao;	
} EstruturaDados;

int vetor[1000];
int numeroAProcurar;

void * funcao_thread(void * arg){
	
	int thread = *((int*) arg); //Parâmetro -> Representa o número da thread
	
	int i;
	for(i = thread * 200; i < (thread+1) * 200; i++){
		if(vetor[i] == numeroAProcurar){
			
			EstruturaDados * estrutura = (EstruturaDados *) malloc(sizeof(EstruturaDados)); 
			
			estrutura->numeroThread = thread + 1;
			estrutura->posicao = i;
		
			pthread_exit((void *) estrutura);
		}
	}
	
	pthread_exit((void*) NULL);	
	
}

int main() {
	
	//------------- Número que pretende procurar -----------------------
	do {
		printf("Digite o número que pretende procurar? De [0 a 999]\n");
		scanf("%d", &numeroAProcurar);
	} while (numeroAProcurar < 0 && numeroAProcurar > 1000);
	//------------------------------------------------------------------
	
	//----------------- Preenche Vetor ----------------------
	int i;
	for(i=0; i<1000;i++){
		vetor[i] = i;
	}
	//-------------------------------------------------------
	
	//-------------------------------- Criação das Threads -----------------------------------
	pthread_t threads[5];
	int args[5];
	int status;
	
    for(i = 0; i < 5; i++) {
    args[i] = i;
        if((status = pthread_create(&threads[i], NULL, funcao_thread, (void *) &args[i]))) {
            printf("Erro a criar a thread: %d\n", status);
            exit(EXIT_FAILURE);
        }       
    }
    //----------------------------------------------------------------------------------------
 
	EstruturaDados valida; 
	valida.posicao = -1; 
	
	void * estrutura;
	
	//------------------------ Esperar por todas as Threads ------------------------------------
	for(i = 0; i < 5; i++) {
        if((status = pthread_join(threads[i], (void *) &estrutura))) {
            printf("Erro no join da thread: %d\n", status);
            exit(EXIT_FAILURE);
        }
 
		//Valida se a thread encontrou o número a procurar
		if (((EstruturaDados *) estrutura) != NULL) {
			valida = *((EstruturaDados *) estrutura);
			free(estrutura);
			break;
		}       
    }
	
	 if (valida.posicao != -1) {
		printf("\nThread número: %d\nPosição no Vetor: %d\n", valida.numeroThread, valida.posicao);
	}
	//-------------------------------------------------------------------------------------------
	return 0;
}
