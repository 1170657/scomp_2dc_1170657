#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <limits.h>

typedef struct {
	int number; 
	int notaG1;
	int notaG2;
	int notaG3;
	int notaFinal;
} Prova;

Prova provas[300];
int num[2] = {0, 0}, aux[2];
pthread_mutex_t	mux[2], prova[300];
pthread_cond_t cond[2];

//------------------- Cria 300 provas com valores aleatórios para as notas das provas ----------------
void* T1(void* arg){
	
	int i;
	for(i = 0; i < 300; i++){
		provas[i].notaG1 = rand() % 100 + 1;
		provas[i].notaG2 = rand() % 100 + 1;
		provas[i].notaG3 = rand() % 100 + 1;
		
		pthread_mutex_unlock(&prova[i]); 	
	}
	
	pthread_exit(NULL);
}
//----------------------------------------------------------------------------------------------------

//----------------- Calcula apenas para as provas com índice par ou ímpar ----------------------------
void* T2E3(void* arg){
	int par = *((int *)arg), i;	

	for(i = par; i < 300; i+=2) {

		pthread_mutex_lock(&prova[i]);

		provas[i].notaFinal = (provas[i].notaG1 + provas[i].notaG2 + provas[i].notaG3) / 3; //-> Calcula nota final 

		if(provas[i].notaFinal < 50) {

			pthread_mutex_lock(&mux[1]);

			aux[1]++;

			pthread_mutex_unlock(&mux[1]);

			pthread_cond_signal(&cond[1]);
		
		} else {
			pthread_mutex_lock(&mux[0]);
			aux[0]++;

			pthread_mutex_unlock(&mux[0]);

			pthread_cond_signal(&cond[0]);
		}
	}
	
	pthread_exit(NULL);
}
//----------------------------------------------------------------------------------------------------

//-------------------- Espera enquanto não há provas positivas ------------------------------------
void* T4(void *arg) {
	
	while(num[1] + num[0] < 300) {
		pthread_mutex_lock(&mux[0]);
		
		while(aux[0] == 0) {
			pthread_cond_wait(&cond[0], &mux[0]);
			if (num[1] + num[0] >= 300) {
				pthread_mutex_unlock(&mux[0]);
			break;
			}
		}

		num[0]++;
		aux[0]--;
		pthread_mutex_unlock(&mux[0]);
	}
	
	//Força o T5 a sair do pthread_cond_wait quando ja nao houver mais provas deste tipo (condição negativa pode estar parada infinitamente)
	aux[1]++;
	pthread_cond_signal(&cond[1]);
	pthread_exit(NULL);
}
//-------------------------------------------------------------------------------------------------

//-------------------- Espera enquanto não há provas negativas ------------------------------------
void* T5(void *arg) {

	while(num[1] + num[0] < 300) {
		pthread_mutex_lock(&mux[1]);
	
		while(aux[1] == 0)
			pthread_cond_wait(&cond[1], &mux[1]);
		
		if (num[1] + num[0] >= 300) {
			pthread_mutex_unlock(&mux[1]);
			break;
		}
		
		num[1]++;
		aux[1]--;
		
		pthread_mutex_unlock(&mux[1]);
	}
	
	//Força o T4 a sair do pthread_cond_wait quando ja nao houver mais provas deste tipo (condição positiva pode estar parada infinitamente) 
	aux[0]++;
	pthread_cond_signal(&cond[0]);
	pthread_exit(NULL);
}
//-------------------------------------------------------------------------------------------------

int main() {
	int i;
	srand(time(NULL));
	
	//-------------- Criar métodos de sincronização: mutex e cond ---------------------------
	for (i = 0; i < 300; i++) { //-> criar mutex para cada prova
		pthread_mutex_init(&prova[i], NULL);
		pthread_mutex_lock(&prova[i]);
	}
	
	for(i = 0; i < 2; i++) { //-> criar mutex para pos e neg
		pthread_cond_init(&cond[i], NULL);
		pthread_mutex_init(&mux[i], NULL);
	}
	//---------------------------------------------------------------------------------------
	
	//------------------------------ Criar as Threads ---------------------------------------
	pthread_t thread[5];
	pthread_create(&thread[0],NULL, T1, NULL);
	
	int par = 0;
	pthread_create(&thread[1],NULL, T2E3, (void*)&par);
	int impar = 1;
	pthread_create(&thread[2],NULL, T2E3, (void*)&impar);
	
	pthread_create(&thread[3],NULL, T4, NULL);
	pthread_create(&thread[4],NULL, T5, NULL);
	//---------------------------------------------------------------------------------------
	
	//----------------------------- Espera que acabem --------------------------------------
	for (i = 0; i < 5; i++) {
		pthread_join(thread[i],NULL);
	}
	//---------------------------------------------------------------------------------------	
	
	//--------------------- Terminar todos os mutex e condições -----------------------------	
	for (i = 0; i < 300; i++) 
		pthread_mutex_destroy(&prova[i]);
	for(i = 0; i < 2; i++) {
		pthread_mutex_destroy(&mux[i]);
		pthread_cond_destroy(&cond[i]);
	}
	//---------------------------------------------------------------------------------------	
	
	float notasPositivas = ((float)num[0]/300)*100;
	float notasNegativas = ((float)num[1]/300)*100;
	
	printf("Notas positivas: %.0f\nNotas negativas: %.0f\n", notasPositivas, notasNegativas);

	return 0;
}
