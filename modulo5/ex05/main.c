#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <limits.h>

int vetor[1000];
int menorSaldo, maiorSaldo, saldoMedio;

//------- Encontra o menor saldo -----------
void* procuraDoMenorSaldo() {

	int start = INT_MAX;
	
	int i;
	for (i = 0; i < 1000; i++) {
		if (vetor[i] < start) {
			start = vetor[i];
		}
	}
	
	menorSaldo = start;
	
	pthread_exit((void*)NULL);
}
//------------------------------------------

//------- ENcontra o maior saldo -----------
void* procuraDoMaiorSaldo() {

	int start = 0;
	
	int i;
	for (i = 0; i < 1000; i++) {
		if (vetor[i] > start) {
			start = vetor[i];
		}
	}
	maiorSaldo = start;
	
	pthread_exit((void*)NULL);
}
//------------------------------------------

//------- Calcula a média dos saldos ---------
void* calculoDoSaldoMedio() {
	int soma=0;
	
	int i;
	for (i = 0; i < 1000; i++) {
		soma += vetor[i];
	}
	saldoMedio = soma/1000;
	pthread_exit((void*)NULL);
}
//--------------------------------------------

int main() {
	
	pthread_t threads[3]; //3 Treads
	void *thread_funcao[3] = { procuraDoMenorSaldo, procuraDoMaiorSaldo, calculoDoSaldoMedio }; //Nomes das funções
	
	int i;
	
	//------- Preenchimento do Vetor ----------------
	srand(time(NULL));
	for (i = 0; i < 1000; i++) {
		*(vetor + i) = rand() % 1000;
	}
	//-----------------------------------------------

	//------------------ Criação das 3 Threads -------------------------
	for (i = 0; i < 3; i++){
		pthread_create(&threads[i], NULL, thread_funcao[i], (void*)NULL);
	}
	//------------------------------------------------------------------
	
	//------------ Espera por todas as Threads -------------------------
	for (i = 0; i < 3; i++) {
		pthread_join(threads[i], NULL);
	}
	//-------------------------------------------------------------------
	
	printf("\n---- Estatísticas do saldo de todos os clientes de um Banco ----\n");
	printf("Saldo maior encontrado: %d\n", maiorSaldo);
	printf("Saldo menor encontrado: %d\n", menorSaldo);
	printf("Saldo médio: %d\n", saldoMedio);
	printf("\n");

	return 0;
}
