#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <limits.h>

pthread_mutex_t mux[3];
char cidades[4];

typedef struct {
	char caminho[2];
} destino;

typedef struct {
	int idComboio;
	destino caminho[4];
	int num_coneccoes;
} caminhoDoComboio;

destino conns[3];

//---------------------------------------------------------- Função simuladora Ferroviária ----------------------------------------------------------------------
void* simuladorFerroviario(void *args) {
	caminhoDoComboio * threadCaminho = (caminhoDoComboio*)args;
	destino* cidadeAtual = threadCaminho[0].caminho;
	
	int connPos, timeRand, tempoTotal = 0;

	srand(time(NULL) % (int)pthread_self());

	while (threadCaminho->num_coneccoes != 0) {
		switch (cidadeAtual->caminho[0]) {
		case 'A':
			if (cidadeAtual->caminho[1] == 'B') {
				connPos = 0;
				break;
			}
		case 'B':
			if (cidadeAtual->caminho[1] == 'C') {
				connPos = 1;
				break;
			}
			else if (cidadeAtual->caminho[1] == 'D') {
				connPos = 2;
				break;
			}
		case 'C':
			if (cidadeAtual->caminho[1] == 'B') {
				connPos = 1;
				break;
			}
		case 'D':
			if (cidadeAtual->caminho[1] == 'B') {
				connPos = 2;
				break;
			}
		default:
			printf("A cidade apresentada no caminho não existe!\n");
			pthread_exit((void*)NULL);
		}

		pthread_mutex_lock(mux + connPos);

		timeRand = (rand() % 3) + 2;
		tempoTotal += timeRand;

		printf("\n");
		printf("\nO Comboio %d partiu :::::: Linha[%c - %c]", threadCaminho->idComboio, conns[connPos].caminho[0], conns[connPos].caminho[1]);
		sleep(timeRand);
		printf("\nO Comboio %d chegou! :::::: Linha[%c - %c]\nTempo: %dh", threadCaminho->idComboio, conns[connPos].caminho[0], conns[connPos].caminho[1], timeRand);
		printf("\n");
		
		pthread_mutex_unlock(mux + connPos);

		cidadeAtual++;
		threadCaminho->num_coneccoes--;
	}

	printf("\nO COMBOIO NÚMERO %d TERMINOU O SEU PASSEIO :::::: TEMPO TOTAL DE VIAGEM: %dh\n", threadCaminho->idComboio, tempoTotal);
	
	pthread_exit((void*)NULL);
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

int main() {

	pthread_t threads[3];
	int i;
	caminhoDoComboio caminhos[3];
	
	void * thread_funcao = simuladorFerroviario;

	for (i = 0; i < 3; i++) {
		pthread_mutex_init(mux + i, NULL);
	}

	//------------------- Criação dos dados ----------------------------
	{	
		//---------------- Cria as cidades -----------------------------
		cidades[0] = 'A';
		cidades[1] = 'B';
		cidades[2] = 'C';
		cidades[3] = 'D';
		//--------------------------------------------------------------

		//------------- Cria as Conecções ------------------------------
		conns[0].caminho[0] = *cidades;			// A - B -> Conecção 0
		conns[0].caminho[1] = *(cidades + 1);
		conns[1].caminho[0] = *(cidades + 1);	// B - C -> Conecção 1
		conns[1].caminho[1] = *(cidades + 2);
		conns[2].caminho[0] = *(cidades + 1);	// B - D -> Conecção 2
		conns[2].caminho[1] = *(cidades + 3);
		//--------------------------------------------------------------

		//------------- Cria os caminhos do Comboio -------------------
		caminhos[0].idComboio = 1;
		caminhos[0].caminho[0] = conns[0];
		caminhos[0].caminho[1] = conns[1];
		caminhos[0].num_coneccoes = 2;

		caminhos[1].idComboio = 2;
		caminhos[1].caminho[0] = conns[0];
		caminhos[1].caminho[1] = conns[2];
		caminhos[1].num_coneccoes = 2;

		caminhos[2].idComboio = 3;
		caminhos[2].caminho[0] = conns[0];
		caminhos[2].caminho[1] = conns[2];
		caminhos[2].caminho[2] = conns[2];
		caminhos[2].num_coneccoes = 3;
		//--------------------------------------------------------------
	}
	//------------------------------------------------------------------
	
	//----------------------- Cria as Threads ----------------------------------
	for (i = 0; i < 3; i++){
		pthread_create(&threads[i], NULL, thread_funcao, (void*)(caminhos + i));
	}
	//--------------------------------------------------------------------------
	
	//---------------------- Esperam que acabem ---------------------------------
	for (i = 0; i < 3; i++) {
		pthread_join(threads[i], NULL);
	}
	//--------------------------------------------------------------------------

	for (i = 0; i < 3; i++) {
		pthread_mutex_destroy((mux + i));
	}
	
	return 0;
}
