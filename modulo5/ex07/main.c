#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <limits.h>

typedef struct {
	int numeros[5];
} EstruturaDados;

int estatisticas[49];

EstruturaDados sorteio;
EstruturaDados chaves[1000];

pthread_mutex_t mux = PTHREAD_MUTEX_INITIALIZER;;

//------ Conta o número de vezes que um número foi sorteado --------
void* stats(void *args) {
	
	EstruturaDados * estrutura = (EstruturaDados*)args;

	int i, j, k;
	
	for (i = 0; i < (1000/10); i++) {
		
		for (j = 0; j < 5; j++) {
			for (k= 0; k < 5; k++) {
				
				if (estrutura[i].numeros[j] == sorteio.numeros[k]) {
					
					pthread_mutex_lock(&mux);
					
					estatisticas[sorteio.numeros[k]-1]++;
					
					pthread_mutex_unlock(&mux);
					break;
				}
			}
		}
	}
	
	pthread_exit(NULL);
}
//--------------------------------------------------------------------------------------

int main() {

	void * thread_funcao = stats;
	pthread_t threads[10];
	
	pthread_mutex_init(&mux, NULL);
	
	int i, j;
	
	//----------------- Preenche todas as chaves ---------------------------
	srand(time(NULL));
	for (i = 0; i < 1000; i++) {
		for (j = 0; j < 5; j++) {
			
			chaves[i].numeros[j] = (rand() % (49 - 1)) + 1;

			int k = j - 1;
			while (k >= 0) {
				if (chaves[i].numeros[j] == chaves[i].numeros[k]) {
					j--;
					break;
				}
				k--;
			}
		}
	}
	
	for (j = 0; j < 5; j++) {
		
		sorteio.numeros[j] = (rand() % (49 - 1)) + 1;
		
		int k = j - 1;
		while (k > -1) {
			if (sorteio.numeros[j] == sorteio.numeros[k]) {
				j--;
				break;
			}
			k--;
		}
	}

	//--------------- Criação das 10 Threads ------------------------------
	for (i = 0; i < 10; i++) {
		pthread_create(&threads[i], NULL, thread_funcao, (void*)(chaves + i));
	}
	//---------------------------------------------------------------------
	
	//-------------- Espera que as Threads acabem -------------------------
	for (i = 0; i < 10; i++) {
		pthread_join(threads[i], NULL);
	}
	
	pthread_mutex_destroy(&mux);
	//---------------------------------------------------------------------
	
	printf("\n---------------------- Estatística do TOTOLOTO -----------------------\n");
	for (i = 0; i < 5; i++) {
		printf("[Número %02d]\t|   Quantidade de vezes que foi sorteado: %02d   | (%.2f%%)\n", sorteio.numeros[i], estatisticas[sorteio.numeros[i] - 1], (float)((float)estatisticas[sorteio.numeros[i] - 1] / (float) 500));
	}

	return 0;
}
