#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <limits.h>

typedef struct {
	int id_h;
	int id_p;
	int p;
} EstruturaProduto;

typedef struct {
	int id_p;
	int p;
} Supermercado;

pthread_t threads[6];

EstruturaProduto vec[10000];
Supermercado vec1[10000];
Supermercado vec2[10000];
Supermercado vec3[10000];

pthread_mutex_t mutex1;
pthread_mutex_t mutex2;
pthread_mutex_t mutex3;

pthread_cond_t cond_var;

float mediaBaixa = INT_MAX;
int cont = 0, h=0, k, l, m;

void * thread_funcao(void *arg) {
	
	int s1 = 0, o = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, p = 0, q = 0, r = 0, t = 0;
	float media1, media2, media3, media4, media5, mediaTotal;
	
	int i = *((int*) arg);
	int j; 
	
	if (i < 3) {
		
		for (j = i * (10000 / 3); j < (i + 1) * (10000 / 3); j++) {
			if (vec[j].id_h == 0) {
				vec1[k].id_p = vec[j].id_p;
				vec1[k].p = vec[j].p;
				k++;
				
				pthread_mutex_lock(&mutex1);
				cont++;
				pthread_mutex_unlock(&mutex1);
				
			} else if (vec[j].id_h == 1) {
				vec2[l].id_p = vec[j].id_p;
				vec2[l].p = vec[j].p;
				l++;
				
				pthread_mutex_lock(&mutex1);
				cont++;
				pthread_mutex_unlock(&mutex1);
				
			} else if (vec[j].id_h == 2) {
				vec3[m].id_p = vec[j].id_p;
				vec3[m].p = vec[j].p;
				m++;
				
				pthread_mutex_lock(&mutex1);
				cont++;
				pthread_mutex_unlock(&mutex1);
				
			}
		}
		
		pthread_mutex_lock(&mutex2);
		
		if (cont == 9999) {
			pthread_cond_broadcast(&cond_var);
		}
		
		pthread_mutex_unlock(&mutex2);
		
	} else {
		pthread_mutex_lock(&mutex2);
		while (cont < 9999) {
			pthread_cond_wait(&cond_var, &mutex2);
		}
		pthread_mutex_unlock(&mutex2);
		
		if (i == 3) {
			
			for (j = 0; j < k; j++) {
				
				if (vec1[j].id_p == 0) {
					s1 = s1 + vec1[j].p;
					o++;
				} else if (vec1[j].id_p == 1) {
					s2 = s2 + vec1[j].p;
					p++;
				} else if (vec1[j].id_p == 2) {
					s3 = s3 + vec1[j].p;
					q++;
				} else if (vec1[j].id_p == 3) {
					s4 = s4 + vec1[j].p;
					r++;
				} else if (vec1[j].id_p == 4) {
					s5 = s5 + vec1[j].p;
					t++;
				}
				
			}
			
			if (o != 0 && p != 0 && q != 0 && r != 0 && t != 0) {
				
				media1 = s1 / o;
				media2 = s2 / p;
				media3 = s3 / q;
				media4 = s4 / r;
				media5 = s5 / t;
				mediaTotal = (media1 + media2 + media3 + media4 + media5) / 5;
				
			}
			pthread_mutex_lock(&mutex3);
			if (mediaTotal < mediaBaixa) {
				h = 0;
				mediaBaixa = mediaTotal;
			}
			
			pthread_mutex_unlock(&mutex3);
			
		} else if (i == 4) {

			for (j = 0; j < l; j++) {
				if (vec2[j].id_p == 0) {
					s1 = s1 + vec2[j].p;
					o++;
				} else if (vec2[j].id_p == 1) {
					s2 = s2 + vec2[j].p;
					p++;
				} else if (vec2[j].id_p == 2) {
					s3 = s3 + vec2[j].p;
					q++;
				} else if (vec2[j].id_p == 3) {
					s4 = s4 + vec2[j].p;
					r++;
				} else if (vec2[j].id_p == 4) {
					s5 = s5 + vec2[j].p;
					t++;
				}
			}
			if (o != 0 && p != 0 && q != 0 && r != 0 && t != 0) {
				
				media1 = s1 / o;
				media2 = s2 / p;
				media3 = s3 / q;
				media4 = s4 / r;
				media5 = s5 / t;
				mediaTotal = (media1 + media2 + media3 + media4 + media5) / 5;
				
			}

			pthread_mutex_lock(&mutex3);
			
			if (mediaTotal < mediaBaixa) {
				h = 1;
				mediaBaixa = mediaTotal;
			}
			pthread_mutex_unlock(&mutex3);
			

		} else {
			
			for (j = 0; j < l; j++) {
				if (vec3[j].id_p == 0) {
					s1 = s1 + vec3[j].p;
					o++;
				} else if (vec3[j].id_p == 1) {
					s2 = s2 + vec3[j].p;
					p++;
				} else if (vec3[j].id_p == 2) {
					s3 = s3 + vec3[j].p;
					q++;
				} else if (vec3[j].id_p == 3) {
					s4 = s4 + vec3[j].p;
					r++;
				} else if (vec3[j].id_p == 4) {
					s5 = s5 + vec3[j].p;
					t++;
				}

			}
			
			if (o != 0 && p != 0 && q != 0 && r != 0 && t != 0) {
				
				media1 = s1 / o;
				media2 = s2 / p;
				media3 = s3 / q;
				media4 = s4 / r;
				media5 = s5 / t;
				mediaTotal = (media1 + media2 + media3 + media4 + media5) / 5;
				
			}
			
			pthread_mutex_lock(&mutex3);
			
			if (mediaTotal < mediaBaixa) {
				h = 2;
				mediaBaixa = mediaTotal;
			}
			pthread_mutex_unlock(&mutex3);
		}
	}
	
	pthread_exit(NULL);
	
}

int main(void) {
	srand(time(NULL));
	
	int args[6], i;
	
	//-------------- Criar métodos de sincronização: mutex e cond ---------------------------
	pthread_mutex_init(&mutex2, NULL);
	pthread_mutex_init(&mutex1, NULL);
	pthread_mutex_init(&mutex3, NULL);
	
	pthread_cond_init(&cond_var, NULL);
	//---------------------------------------------------------------------------------------

	for (i = 0; i < 10000; i++) {
		vec[i].id_h = rand() % 3;
		vec[i].id_p = rand() % 5;
		vec[i].p = rand() % 50;
	}
	
	//--------------------- Criação das Threads -------------------------
	for (i = 0; i < 6; i++) {
		args[i] = i;
		pthread_create(&threads[i], NULL, thread_funcao, (void*) &args[i]);
	}
	//--------------------------------------------------------------------
	
	//----------------------------- Espera que acabem --------------------------------------
	for (i = 0; i < 6; i++) {
		pthread_join(threads[i], NULL);
	}
	//---------------------------------------------------------------------------------------	
	
	printf("O SuperMercado %d apresenta o preço mais baixo de %f€.\n", h+1, mediaBaixa); 
			
	return 0;
}
