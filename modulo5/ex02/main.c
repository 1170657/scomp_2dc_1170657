#include <sys/stat.h>
#include <fcntl.h> 
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>
#include <pthread.h>


typedef struct{
	int numero;
	char nome[50];
	char morada[50];
} EstruturaDados;


void * thread_funcao(void* argumento) {
	EstruturaDados * estrutura = (EstruturaDados*) argumento;
	printf("\n| NÚMERO: %d | NOME: %s | ENDEREÇO: %s |\n", estrutura->numero, estrutura->nome, estrutura->morada);
	pthread_exit ((void*)NULL); //termina o chamamento da thread 
}


int main() {
	
	int i;
	
	char * nomes[5] = {"Filipa Pereira", "Hugo Soares", "Helena Pinto", "Ivone Soares", "Pedro Soares"};
	char * moradas[5] = {"Avenida dos Aliados", "Rua do Almada", "Avenida da Boavista", "Rua da Constituição", "Rua da Galeria de Paris"};
	
	EstruturaDados array[5];
	
	pthread_t thread_id[5];
	
	
	for(i=0; i < 5; i++) {
		array[i].numero = i+1;
		sprintf(array[i].nome, "%s", nomes[i]);
		sprintf(array[i].morada,"%s", moradas[i]);
	}


	for(i=0; i < 5; i++) {
		pthread_create(&thread_id[i], NULL, thread_funcao, (void*)&array[i]);
	}
	
	for(i=0; i < 5; i++) {
		pthread_join(thread_id[i], NULL);
	}
	
	return 0;
}
