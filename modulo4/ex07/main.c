#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <wait.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>

int main(){
	pid_t pid;
	int i = 0;
	sem_t *sem[4];
	char * names[4] = {"/sem_ex07_1", "/sem_ex07_2", "/sem_ex07_3", "/sem_ex07_4"};

	for(i = 0; i < 4; i++){
		if((sem[i] = sem_open(names[i], O_CREAT | O_EXCL, 0644, 0)) == SEM_FAILED){
			printf("Erro no sem_open()\n");
			sem_unlink(names[i]);
			exit(1);
		}
	}
	
	sem_post(sem[0]);
	
	for(i = 0; i < 3; i++){
		pid = fork();
		if(pid == 0){
			break;
		}
	}
	
	if(pid == 0){
		
		if(i == 0){
			
			sem_wait(sem[0]);
			printf("Sistemas ");
			fflush(stdout);
			
			sem_post(sem[1]);
			
			sem_wait(sem[0]);
			printf("a ");
			fflush(stdout);
			
			sem_post(sem[1]);
			
		} else if(i == 1){
			
			sem_wait(sem[1]);
			printf("de ");
			fflush(stdout);
			
			sem_post(sem[2]);
			
			sem_wait(sem[1]);
			printf("melhor ");
			fflush(stdout);
			sem_post(sem[2]);
			
		} else if(i == 2){
			
			sem_wait(sem[2]);
			printf("Computadores - ");
			fflush(stdout);
			
			sem_post(sem[0]);
			
			sem_wait(sem[2]);
			printf("disciplína! \n");
			fflush(stdout);
			sem_post(sem[3]);
		}
		
	} else if(pid > 0){
		sem_wait(sem[3]);
		
		for(i = 0; i < 4; i++){
			sem_unlink(names[i]);
		}
	}
	
	return 0;
}
