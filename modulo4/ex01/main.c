#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <wait.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>

int main(){  
	 
   //-> Cria semáforo com o VALOR 1.
   sem_t *sem; 
   if ((sem = sem_open("ex01", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED){
		perror("Error in sem_open()");
		exit(1);
	}
   
   char str[100] = "";
   char arr[200][100];
   int cont = 0, i, j;
   
   FILE * f1;
   FILE * f2;
   FILE * f3;
   
   f1 = fopen ("Output.txt", "w");
   
   pid_t pid;
   //-> Aqui vai criar os 8 novos processos
   for(i = 0; i < 8; i++){
		pid = fork();
		
		if(pid == 0){
			
			//-> Semáforo passa a 0 isto para aceder um filho de cada vez.
			sem_wait(sem);
			
			f2 = fopen("Numbers.txt", "r"); //-> LEITURA 200 números
			
			while (fgets(str, 100, f2) != NULL){
				str[strlen(str) - 2] = '\0';
				strcpy(arr[cont], str);
				cont++;
			}
			
			fclose(f2);
			
			//-> Escreve no ficheiro "Output.txt"
			for(j = 0; j < 200; j++){
				if(j == 0){
					fprintf(f1, "\n Processo filho nrº%d\n", i);
				}
				
				fprintf(f1, "\n%s\n", arr[j]);
			}
			
			sem_post(sem);
			//-> Semáforo passa a 1 para o próximo filho poder entrar.
			
			exit(0);
		}
	}
	
	fclose(f1);
	sem_unlink("ex01");
   
   //-> Espera que os processos filhos terminem 
   if(pid > 0){
	   int status;
	   for(i = 0; i < 8; i++){
			wait(&status);
			if(!WIFEXITED(status)){
				perror("Erro no pai!\n");
				exit(-1);
			}
		}
	}
	
	//-> O processo pai impime o conteúdo do ficheiro "Output.txt"
	f3 = fopen ("Output.txt", "r");
	while (fgets(str, 100, f3) != NULL){
		printf("%s", str);
	}
	
	fclose(f3);
	
   return 0;
}
