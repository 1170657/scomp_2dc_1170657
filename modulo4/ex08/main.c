#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <semaphore.h>

typedef struct {
	int sCont;
	int cCont;
} data;

int main() {
	pid_t pid;
	sem_t* sem;
	setbuf(stdout, NULL);
	
	//---------------Memória Partilhada-----------------------------------------------------
	int fd = shm_open("/shm_ex08", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	if(fd == -1){
		perror("Erro no shm_open!\n");
		exit(1);
	}
	
	if(ftruncate(fd, sizeof(data)) == -1){
		perror("Erro no ftruncate!\n");
		exit(1);
	}
	 
	data * dados = (data *) mmap(NULL, sizeof(data), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	if(dados == MAP_FAILED){
		perror("Erro no mmap!\n");
		exit(1);
	}
	//----------------------------------------------------------------------------------------
	
	//-------------------Criação de Semáforo-------------------------------------------------
	if((sem = sem_open("sem_ex08", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED){
			printf("Erro no sem_open()\n");
			sem_unlink("sem_ex08");
			exit(1);
	}
	//---------------------------------------------------------------------------------------
	
	pid = fork();
	
	if (pid == 0) {
		
		while(1) {
			if (sem_trywait(sem)) {
				
				if (dados->sCont + dados->cCont == 10) {
					sem_post(sem);
					exit(0);
				}
				
				if ((dados->sCont - dados->cCont) < 1) {
					printf("S");
					dados->sCont++;
				}
				
				sem_post(sem);	
			}
		}
	} else if (pid > 0) {
		
		while(1){
			if (sem_trywait(sem)){
				
				if (dados->sCont + dados->cCont == 10){
					sem_post(sem);
					printf("\n");
	
					sem_unlink("/sem_ex08");
					shm_unlink("/shm_ex08");
					break;
				}
				
				if ((dados->cCont - dados->sCont) < 1 ){
					printf("C");
					dados->cCont++;
				}
				
				sem_post(sem);
			}
		}
	} else if (pid == -1) {
		perror("Erro no fork\n");
		exit(1);
	}
	
	return 0;
}
