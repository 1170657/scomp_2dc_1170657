#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <semaphore.h>

typedef struct{
	int number;
	char name[50];
	char address[50];
}data_records;

typedef struct{
	int cont;
	data_records data_records[100];
}data;

int main(){
	int i = 0;
	sem_t *sem;
	
	int fd = shm_open("/shm_ex10", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	if(fd == -1){
		perror("Erro no shm_open!\n");
		exit(0);
	}
	
	if(ftruncate(fd, sizeof(data)) == -1){
		perror("Erro no ftruncate\n");
		exit(0);
	}
	
	data *dados = mmap(NULL, sizeof(data), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	if(dados == NULL){
		printf("Erro no mmap\n");
		exit(0);
	}
	
	sem = sem_open("/sem_ex10", O_CREAT, 0644, 1);
	
	if(sem == SEM_FAILED){
		printf("Erro no sem_open\n");
		return 0;
	}
	
	sem_wait(sem);
	
	if(dados->cont > 0){
		for(i = 0; i < dados->cont; i++){
			data_records *registos = &(dados->data_records[i]);
			printf("Registo:\nNúmero: %d  Nome: %s  Morada: %s\n", registos->number, registos->name, registos->address);
		}
	} else if(dados->cont == 0){
		printf("Não existem registos.\n");
	}
	
	sem_post(sem);
	
	sem_unlink("/sem_ex10");
	
	munmap(dados, sizeof(data));
	close(fd);
	
	return 0;
}
