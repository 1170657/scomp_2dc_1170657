#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <semaphore.h>

typedef struct {
	int indice;
	char vec[50][80];
} data;
 
int main () {

	//-----------Memória Partilhada-----------------------------------
	int fd = shm_open("/shm_ex03", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	if(fd == -1){
		perror("Erro no shm_open!\n");
		exit(1);
	}

    if(ftruncate(fd, sizeof(data)) == -1){
		perror("Erro no ftruncate!\n");
		exit(1);
	}
    
    data * dados = (data *) mmap(NULL, sizeof(data), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    
    if(dados == MAP_FAILED){
		perror("Erro no mmap!\n");
		exit(1);
	}
	
	// ---------------------------------------------------------------
	
	
	//-> Cria um Semáforo com o VALOR 1
	sem_t *sem;
	if((sem = sem_open("sem_ex03", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED){
			printf("Erro no sem_open()\n");
			sem_unlink("sem_ex03");
			exit(1);
	}
	
	int i = 0;
	
	pid_t pid;
	for(i = 0; i < 20; i++) {
		pid = fork();
		
		if(pid == -1) {
			
			perror("Erro no fork\n");
			exit(1);
			
		} else if(pid == 0) {
			
			srand(time(NULL));
			int tempoEspera = rand() % (1 + (5 - 1));
			
			sem_wait(sem);
	
			sprintf(dados->vec[dados->indice], "EU SOU O PAI com o PID %d", getpid());
			dados->indice++;
			
			if(dados->indice == 50) {
				dados->indice = 0;
			}
	
			printf("Processo à espera %d segundos.\n", tempoEspera);
			sleep(tempoEspera);
		
			sem_post(sem);
			
			exit(0);
		} else {
			wait(NULL);
		}
	}
	
	sem_wait(sem);
	
	for(i = 0; i < 50; i++) {
		printf("%s\n", dados->vec[i]);
	}
	
	sem_post(sem);
	
	if(munmap(dados, sizeof(data)) == -1){
		perror("Erro no munmap\n");
		exit(1);
	}
	
	if(close(fd) == -1){
		perror("Erro no close\n");
		exit(1);
	}
	
	if(shm_unlink("/shm_ex03") == -1) {
		perror("Erro no shm_unlink\n");
		exit(1);
	}
	
    sem_unlink("sem_ex03");
    
	return 0;
}
