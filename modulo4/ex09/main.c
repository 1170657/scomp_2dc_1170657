#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <semaphore.h>
	
void buy_chips(){
	printf("Batatas!\n");
}	

void buy_beer(){
	printf("Cerveja!\n");
}	

void eat_and_drink(){
	printf("Comer!\n");
}	

	
int main(void){
	
	pid_t pid;
	sem_t *sem[2];
	int values[2] = {0, 1};
	char *names[2] = {"/sem_ex09_1", "/sem_ex09_2"};
	int i = 0;
	
	for(i = 0; i < 2; i++){
		if((sem[i]  = sem_open(names[i],  O_CREAT | O_EXCL, 0644, values[i])) == NULL){
			printf("Erro no sem_open()\n");
			sem_unlink(names[i]);
			exit(1);
		}
	}
	
	pid = fork();
	
	if (pid == 0){
		buy_chips();
		sem_post(sem[0]);
		if(sem_trywait(sem[1])) {
			eat_and_drink();
		}
		sem_post(sem[0]);
		exit(0);
	} else if (pid > 0){
		sem_wait(sem[0]);
		buy_beer();
		if(sem_trywait(sem[1])) {
			eat_and_drink();
		}
	} else if (pid < 0){
		perror("Erro no fork\n");
		exit(1);
	}
	
	sem_wait(sem[0]);
	
	for(i = 0; i < 2; i++){
		sem_unlink(names[i]);
	}
	
	return 0;
}
