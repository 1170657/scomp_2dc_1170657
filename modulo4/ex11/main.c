#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <semaphore.h>

int main(){
	int i = 0, j = 0, status = 0;
	pid_t pid;
	
	sem_t *sem[3];
	char *names[3] = {"/sem_ex11_0","/sem_ex11_1","/sem_ex11_2"};
	
	char bilheteOut[10] = "";
	char bilheteIn[10] = "";
	
	//------------Criação dos Semáforos--------------------------------------
	for(i = 0; i < 3; i++){
		if((sem[i]  = sem_open(names[i],  O_CREAT | O_EXCL, 0644, 1)) == NULL){
			printf("Erro no sem_open()\n");
			sem_unlink(names[i]);
			exit(1);
		}
	}
	//----------------------------------------------------------------------- 
	
	for(i = 0; i < 200; i++) {
		pid = fork();
		
		//------------Simulação de bilhetes--------------------------
		if(i < 10){
			sprintf(bilheteOut, "B0000%d", i);
			sprintf(bilheteIn, "A0000%d", i);
		} else if(i > 9 && i < 100){
			sprintf(bilheteOut, "B000%d", i);
			sprintf(bilheteIn, "A000%d", i);
		} else {
			sprintf(bilheteOut, "B00%d", i);
			sprintf(bilheteIn, "A00%d", i);
		}
		//--------------------------------------------------------
		
		if(pid == 0) {
			while(1) {
				for(j = 0; j < 3; j++) {
					
					//A tentar sair por uma das 3 portas.
					if(sem_trywait(sem[j]) == 0) {
						printf("Bilhete %s terminou a sua viagem. Saída: %d.\n", bilheteOut, j + 1);
						sem_post(sem[j]);
						break;
					} else if(sem_trywait(sem[j]) != 0) {
						continue;
					}
				}
				
				//A tentar entrar por uma das 3 portas.
				for(j = 0; j < 3; j++) {
					if(sem_trywait(sem[j]) == 0) {
						printf("Bilhete %s começou a sua viagem. Entrada: %d.\n", bilheteIn, j + 1);
						sem_post(sem[j]);
						exit(0);
					} else if(sem_trywait(sem[j]) != 0) {
						continue;
					}
				}
			}
			
		} else if (pid > 0){
			wait(&status);
			if(!WIFEXITED(status)){
				perror("Processo terminou com erro!\n");
				for(i = 0; i < 3; i++){
					sem_unlink(names[i]);
				}
				exit(1);
			}
		} else if (pid < 0){
			perror("Erro no fork()!\n");
			exit(1);
		}
	}
	
	for(i = 0; i < 3; i++){
		sem_unlink(names[i]);
	}
	
	return 0;
}
