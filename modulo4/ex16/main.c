#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <wait.h>
#include <time.h>

typedef struct{
	int viaEsquerda;
	int viaDireita;
}info;

int main()
{
	pid_t p;
	int i,fd;
	
	info* shared;
	
	sem_t* esquerda;
	sem_t* direita;
	sem_t* acesso;
	sem_t* ponte;
	
	//----------------Criação de Semáforos------------------------------
	esquerda = sem_open("esquerda", O_CREAT, 0600, 0);
	if(esquerda==SEM_FAILED){
		perror("Erro!\n");
		exit(0);
	}
	
	direita = sem_open("direita", O_CREAT, 0600, 0);
	if(direita==SEM_FAILED){
		perror("Erro!\n");
		exit(0);
	}
	
	acesso = sem_open("acesso", O_CREAT, 0600, 1);
	if(acesso==SEM_FAILED){
		perror("Erro!\n");
		exit(0);
	}
	
	ponte = sem_open("ponte", O_CREAT, 0600, 1);
	if(ponte==SEM_FAILED){
		perror("Erro!\n");
		exit(0);
	}
	//------------------------------------------------------------------
	
	//--------------Memória Partilhaa-----------------------------------
	if((fd=shm_open("/ex16", O_CREAT|O_EXCL|O_RDWR, 0600)) == -1){
		perror("Erro!\n");
		exit(0);
	}
	
	if((ftruncate(fd,sizeof(info)))==-1){
		perror("Erro!\n");
		exit(0);
	}
	
	shared = (info*) mmap(NULL, sizeof(info), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if(shared == NULL){
		perror("Erro!\n");
		exit(0);
	}
	//--------------------------------------------------------------------
	
	shared->viaEsquerda = 4; //4 carros
	shared->viaDireita = 3;	// 3 carros
	
	p=fork();
	
	if(p==0){
		
		//direita -> 3 carros
		for(i=0; i<3; i++){
			
			p=fork();
			
			if(p==0){
				
				if(i==0){
					
					while((sem_trywait(ponte))!= 0); //fica preso aqui porque nos dos lados da ponte alguém está a passar
					
					sem_post(direita); //-> Este semáforo apenas serve para bloquear os restantes carros até o 1º sair do while.
					
				} else {
					sem_wait(direita);
					sem_post(direita);
				}
				
				sleep(30); //30min para atravesar a ponte
				
				//---------Passar a Ponte na via da direita---------------
				sem_wait(acesso); //-> Este semáforo é o que simula o acesso à ponte 
				
				printf("--- Via da Direita livre --- >\n");
				
				shared->viaDireita = shared->viaDireita-1;
				
				if(shared->viaDireita==0){
					
					sem_post(ponte);
					
				}
				
				sem_post(acesso);
				//--------------------------------------------------------
				
				if((munmap(shared,sizeof(info)))==-1){
					perror("Erro!\n");
					exit(0);
				}
				
				if((close(fd))==-1){
					perror("Erro!\n");
					exit(0);
				}
				
				exit(0);
			}
		}
		
		for(i=0;i<3;i++){
			wait(NULL);
		}
		if((munmap(shared,sizeof(info)))==-1){
			perror("Erro!\n");
			exit(0);
		}
		if((close(fd))==-1){
			perror("Erro!\n");
			exit(0);
		}
		exit(0);
		
	} else {
		
		//esquerda -> 4 carros
		for(i=0; i<4; i++){
			
			p=fork();
			
			if(p==0){
				
				if(i==0){
					
					while((sem_trywait(ponte))!= 0);
					
					sem_post(esquerda);
					
				} else {
					
					sem_wait(esquerda);
					sem_post(esquerda);
					
				}
				
				sleep(30);
				
				sem_wait(acesso);
				
				printf("--- Via da Esquerda livre --- >\n");

				shared->viaEsquerda = shared->viaEsquerda-1; //Decrementa aqui pois um carro já passou.
				
				if(shared->viaEsquerda==0){
					
					sem_post(ponte); //Quando todos os carros da esquerda tiverem passado, ele desbloqueia a ponte do lado direito.
					
				}
				
				sem_post(acesso);
				
				if(shared->viaDireita==0){
					
					sem_post(ponte);
					
				}
				
				sem_post(acesso);
				
				if((munmap(shared,sizeof(info)))==-1){
					perror("Erro a fechar a memória partilhada!\n");
					exit(0);
				}
				
				if((close(fd))==-1){
					perror("cErro!\n");
					exit(0);
				}
				
				exit(0);
			}
		}
		
		for(i=0;i<5;i++){
			wait(NULL);
		}
		
		if((munmap(shared,sizeof(info)))==-1){
			perror("Erro!\n");
			exit(0);
		}
		
		if((close(fd))==-1){
			perror("Erro!\n");
			exit(0);
		}
		
		if((shm_unlink("/ex16"))==-1){
			perror("Erro!\n");
			exit(0);
		}
		
		if((sem_unlink("acesso"))==-1){
			perror("Erro!\n");
			exit(0);
		}
		
		if((sem_unlink("direita"))==-1){
			perror("Erro!\n");
			exit(0);
		}
		
		if((sem_unlink("esquerda"))==-1){
			perror("Erro!\n");
			exit(0);
		}
		
		if((sem_unlink("ponte"))==-1){
			perror("Erro!\n");
			exit(0);
		}
	}
	
	return 0;
}
