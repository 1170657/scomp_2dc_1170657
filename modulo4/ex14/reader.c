#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <semaphore.h>

typedef struct{
	int nr_readers;
	char string[100];
} data;

int main(){
	int i;
	sem_t *sem[2];
	char *names[2] = {"/sem_ex14_1", "/sem_ex14_2"};
	
	for(i = 0; i < 2; i++){
		sem_unlink(names[i]);
	}
	
	//------------------Memória Partilhada---------------------------------------------
	int fd = shm_open("/shm_ex14", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	if(fd == -1){
		perror("Erro no shm_open\n");
		exit(0);
	}
	
	data *dados = mmap(NULL, sizeof(data), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);	
	
	if(ftruncate(fd, sizeof(data)) == -1){
		perror("Erro no ftruncate\n");
		exit(0);
	}
	
	if(dados == NULL){
		printf("Erro no mmap\n");
		exit(0);
	}
	//-----------------------------------------------------------------------------
	
	//--------------------Criação dos Semáforos----------------------------------
	for(i = 0; i < 2; i++){
		if((sem[i]  = sem_open(names[i],  O_CREAT | O_EXCL, 0644, 1)) == NULL){
			printf("Erro no sem_open()\n");
			sem_unlink(names[i]);
			exit(1);
		}
	}
	//--------------------------------------------------------------------------
	
	if(sem_trywait(sem[0]) == 0) {
		
		dados->nr_readers++;
			
		if(dados->nr_readers == 1){
			sem_wait(sem[1]);
		}
		
		printf("String: %s | Leitores: %d\n", dados->string, dados->nr_readers);
		
		dados->nr_readers--;
			
		if(dados->nr_readers == 0){
			sem_post(sem[1]);
		}
	
	} else {
		printf("Memória bloqueada!\n");
	}

	munmap(dados, sizeof(data));
	close(fd);
	
	return 0;
}
