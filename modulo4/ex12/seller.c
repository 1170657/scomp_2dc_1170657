#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <semaphore.h>

typedef struct{
	int cont;
}data;

int main(){
	int i;
	char *sem_names[20];
	sem_t *sem[20];
	
	int fd = shm_open("/shm_ex12", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	if(fd == -1){
		perror("Erro no shm_open\n");
		exit(0);
	}
	
	if(ftruncate(fd, sizeof(data)) == -1){
		perror("Erro no ftruncate\n");
		exit(0);
	}
	
	data *dados = mmap(NULL, sizeof(data), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	if(dados == NULL){
		printf("Erro no mmap\n");
		exit(0);
	}
	
	for(i = 0; i < 20; i++) {
		sem_names[i] = malloc(100);
		sprintf(sem_names[i], "/sem_ex12_%d", i);
		
		sem[i] = sem_open(sem_names[i], O_CREAT, 0644, 0);
		if(sem[i] == SEM_FAILED){
			printf("Erro no sem_open\n");
			return 0;
		}
	}
	
	dados->cont++;
	
	sem_post(sem[19]);
	
	while(1){
		printf("Bilheteira à espera de um novo cliente.\n");

		sem_wait(sem[18]);
		
		printf("Cliente entrou na bilheteira.\n");
			
		sem_post(sem[dados->cont % 18]);
			
		dados->cont++;
		dados->cont++;
		
		sem_post(sem[19]);
		
		sem_wait(sem[20]);
		
		printf("Cliente nº %d acabou a compra.\n", dados->cont - 1);
	}
	
	for(i = 0; i < 20; i++) {
		sem_unlink(sem_names[i]);
	}
	
	munmap(dados, sizeof(data));
	close(fd);
	
	return 0;
}
