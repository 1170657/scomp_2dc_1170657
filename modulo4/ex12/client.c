#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <semaphore.h>

typedef struct{
	int cont;
}data;

int main(){
	srand(time(NULL));
	int i;
	sem_t *sem[20];
	char *sem_names[20];
	int x = rand() % 10;
	
	int fd = shm_open("/shm_ex12", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	if(fd == -1){
		perror("Erro no shm_open\n");
		exit(0);
	}
	
	if(ftruncate(fd, sizeof(data)) == -1){
		perror("Erro no ftruncate\n");
		exit(0);
	}
	
	data *dados = mmap(NULL, sizeof(data), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	if(dados == NULL){
		printf("Erro no mmap\n");
		exit(0);
	}
	
	for(i = 0; i < 20; i++) {
		sem_names[i] = malloc(100);
		sprintf(sem_names[i], "/sem_ex12_%d", i);
		
		sem[i] = sem_open(sem_names[i], O_CREAT, 0644);
		if(sem[i] == SEM_FAILED){
			printf("Erro no sem_open\n");
			return 0;
		}
	}
	
	printf("Client: Waiting for seller.\n");
	
	sem_wait(sem[19]);
		
	printf("Client: Ticket assigned: %d\n", dados->cont);
		
	sem_post(sem[18]);

	sem_wait(sem[dados->cont % 18]);

	printf("Client: Waiting %d seconds.\n", x);
	sleep(x);
	
	sem_post(sem[20]);
	
	close(fd);
	
	return 0;
}
