#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <wait.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>

int main(){  
   sem_t *sem[8];
   char * names[8] = {"/sem_00", "/sem_01", "/sem_02", "/sem_03", "/sem_04", "/sem_05", "/sem_06", "/sem_07"};
   
   //-> Criam-se 8 semáforos com o VALOR 0
   int i;
   for(i = 0; i < 8; i++){
	   if ((sem[i] = sem_open(names[i], O_CREAT | O_EXCL, 0644, 0)) == SEM_FAILED){
			perror("Error in sem_open()");
			exit(1);
		}
	}

   char str[100] = "";
   char arr[200][100];
   int cont = 0, j;
   
   FILE * f1;
   FILE * f2; 
   FILE * f3;
   
   f1 = fopen ("Output.txt", "w");
   
   //-> Incrementa-se o 1ºSemáforo 
   sem_post(sem[0]);
   
   pid_t pid;
   for(i = 0; i < 8; i++){
		pid = fork();
		
		if(pid == 0){
			
			sem_wait(sem[i]);
			
			f2 = fopen("Numbers.txt", "r");
			
			while (fgets(str, 100, f2) != NULL){
				str[strlen(str) - 2] = '\0';
				strcpy(arr[cont], str);
				cont++;
			}
			
			fclose(f2);
			
			for(j = 0; j < 200; j++){
				if(j == 0){
					fprintf(f1, "\n Processo filho nrº%d\n", i);
				}
				
				fprintf(f1, "\n%s\n", arr[j]);
			}
			
			if(i < 7){
				sem_post(sem[i+1]);
			}
			exit(0);
		}		
	}
	
	fclose(f1);
	
	for (i = 0; i < 8; ++i){
		sem_unlink(names[i]);
	}
   
   if(pid > 0){
	   int status;
	   for(i = 0; i < 8; i++){
			wait(&status);
			if(!WIFEXITED(status)){
				perror("Erro no pai!\n");
				exit(1);
			}
		}
	}
	
	f3 = fopen ("Output.txt", "r");
	while (fgets(str, 100, f3) != NULL){
		printf("%s", str);
	}
	
	fclose(f3);
	
   return 0;
}
