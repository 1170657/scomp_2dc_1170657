#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/wait.h>
#include "semaphore.h"

#define CAPACIDADE_MAXIMA 300 // O cinema tem a capacidade máxima de 300 espectadores.
#define VISITANTES 400		 //	 Número de visitante escolhido.

typedef struct{
	int espectVIPs;		  // Espectadores com pirioridade em relação aos outros.
	int espectEspeciais; // Os três tipos de espectadores.
	int espectNormais;
} lotacaoCinema;

int main(void){
	
	sem_t * espera;				  // Inicializado a 0.
	sem_t * sem_espectNormal;	 // Inicializado a 1.
	sem_t * sem_espectEspecial; // Um semáforo para cada espectador.
	sem_t * sem_espectVIP;	   // Inicializado a 0.
	sem_t * sem_capacidade;	  // Este semáforo é inicializado a 300!
	
	pid_t filho;
	
	int descritor, data_size = sizeof(lotacaoCinema), i;
	
	//---------------------------------------Criação de Semáforos----------------------------------------------
	if ((sem_capacidade = sem_open("sem_capacidade_ex17", O_CREAT | O_EXCL, 0644, CAPACIDADE_MAXIMA)) == SEM_FAILED) {
		perror("Erro!");
		exit(EXIT_FAILURE);
	}
	
	if ((sem_espectNormal = sem_open("sem_normal_ex17", O_CREAT | O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("Erro!");
		exit(EXIT_FAILURE);
	}
	
	if ((sem_espectVIP = sem_open("sem_vip_ex17", O_CREAT | O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("Erro!");
		exit(EXIT_FAILURE);
	}
	
	
	if ((sem_espectEspecial = sem_open("sem_especial_ex17", O_CREAT | O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("Erro!");
		exit(EXIT_FAILURE);
	}
	
	if ((espera = sem_open("sem_espera_ex17", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("Erro!");
		exit(EXIT_FAILURE);
	}
	//--------------------------------------------------------------------------
	
	//--------------------------Memória Partilhada-------------------------------------------
	if ((descritor = shm_open("ex17_ficheiro", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR)) < 0) {
		perror("Erro!");
		exit(EXIT_FAILURE);
	}
	
	if (ftruncate(descritor, data_size) < 0) {
		perror("Erro!");
		exit(EXIT_FAILURE);
	}
	

	lotacaoCinema * shared;
	if ((shared = (lotacaoCinema *) mmap(NULL, data_size, PROT_READ | PROT_WRITE,MAP_SHARED, descritor, 0)) == MAP_FAILED) {
		perror("Erro!");
		exit(EXIT_FAILURE);
	}
	//-------------------------------------------------------------------------------------------------
	
	for (i = 0; i < VISITANTES; i++) {
		
		if ((filho = fork()) < 0) {
			
			perror("Erro!");
			exit(EXIT_FAILURE);
			
		} else if (filho == 0) {
			srand(getpid());
			
			int tipoVisitante = rand() % 3; // 0 normal, 1 especial, 2 vip -> Escolhe-se o tipo de Visitante
			
			int capacidade;
			
			sem_getvalue(sem_capacidade, &capacidade);
			
			if(capacidade = 0){ // Só entra aqui quando a capacidade está a 0, ou seja, lotação cheia.
				
				printf("SALA LOTADA!\n");
				
				printf("VISITANTE -> ID: %d e Tipo: %d à espera\n", i, tipoVisitante);
				
				if(tipoVisitante == 2){
					
					sem_wait(espera);
					
					shared->espectNormais = shared->espectNormais + 1; 
					
					sem_post(espera); 
					
					sem_wait(sem_espectNormal); 
					
				}else if(tipoVisitante == 1){
					
					sem_wait(espera);
					
					shared->espectEspeciais = shared->espectEspeciais + 1;
					
					sem_post(espera);
					
					sem_wait(sem_espectEspecial); 
					
				}else if(tipoVisitante == 0){
					
					sem_wait(espera);
					
					shared->espectVIPs = shared->espectVIPs + 1;
					
					sem_post(espera); 
					
					sem_wait(sem_espectVIP);
					
				}
			}
			
			
			//--------------
			sem_wait(sem_capacidade); 
			
			printf("VIISITANTE ->  ID: %d e Tipo: %d está dentro do cinema\n", i, tipoVisitante);
			
			sleep(rand()%5 + 1); //Simula o tempo do espectáculo.
			
			sem_post(sem_capacidade);
			//----------------
			
			
			printf("VISITANTE ->  ID: %d e Tipo: %d está fora do cinema\n", i, tipoVisitante);
			
			sem_getvalue(sem_capacidade, &capacidade);
			
			//-----------------------------------
			
			sem_wait(espera);
			
			//printf("VIP: %d | Especial: %d | Normal: %d\n", shared->espectVIPs, shared->espectEspeciais, shared->espectNormais);
			
			if (shared->espectVIPs > 0) { 
				
				sem_post(sem_espectVIP); 
				
				printf("Entrou um Visitante VIP\n");
				
				shared->espectVIPs = shared->espectVIPs - 1;
				
			} else if (shared->espectEspeciais > 0) {
				
				sem_post(sem_espectEspecial);
				
				printf("Entrou um Visitante Especial\n");
				
				shared->espectEspeciais = shared->espectEspeciais - 1;
				
			} else if (shared->espectNormais > 0){
				
				sem_post(sem_espectNormal);
				
				printf("Entrou um Visitante Normal\n");
				
				shared->espectNormais = shared->espectNormais - 1;
			}
			
			sem_post(espera);
			
			//-----------------------------------------
			
			exit(EXIT_SUCCESS);
		}
	}	
	
	for (i = 0; i < VISITANTES; i++) {
		wait(NULL);
	}
	
	if (sem_unlink("sem_capacidade_ex17") < 0) {
		
		perror("Erro!");
		exit(EXIT_FAILURE);
		
	}
	
	if (sem_unlink("sem_normal_ex17") < 0) {
		
		perror("Erro!");
		exit(EXIT_FAILURE);
		
	}
	
	if (sem_unlink("sem_vip_ex17") < 0) {
		
		perror("Erro!");
		exit(EXIT_FAILURE);
		
	}
	
	if (sem_unlink("sem_especial_ex17") < 0) {
		
		perror("Erro!");
		exit(EXIT_FAILURE);
		
	}
	
	if (sem_unlink("sem_espera_ex17") < 0) {
		
		perror("Erro!");
		exit(EXIT_FAILURE);
		
	}
	
	if (close(descritor) < 0) {
		
		perror("Erro!");
		exit(EXIT_FAILURE);
		
	}
	
	if (shm_unlink("ex17_ficheiro") < 0) {
		
		perror("Erro!");
		exit(EXIT_FAILURE);
		
	}
	
	return 0;
}
