#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <time.h>

int main() {
	pid_t pid;
	int i, lotacao = 0;
	int values[4] = {1, 5, 0, 0};
	char *names[4] = {"/sem_show_in", "/sem_visitantes", "/sem_fim", "/sem_show_out"};
	sem_t* sem[4];
    
    for(i = 0; i < 4; i++){
		sem_unlink(names[i]);
    }
	
	for(i = 0; i < 4; i++){
		if((sem[i] = sem_open(names[i],  O_CREAT | O_EXCL, 0644, values[i])) == NULL){
			printf("Erro no sem_open()\n");
			sem_unlink(names[i]);
			exit(1);
		}
	}

	for(i = 0; i < 10; i++) {
		
		pid = fork();
		
		if(pid == 0) {
			
			while(1) {
                
                printf("O Visitante nrº%d está à espera para entrar!\n", i + 1); // Os visitantes começam por esperar que o semáforo 0 que é iniciado com valor 1 seja decrementado
				
				sem_wait(sem[0]); // O processo decrementa-o. Depois ficam presos aqui.
                
				printf("O Visitante nrº%d acabou de entrar!\n", i + 1); // Entra no espetáculo
                
				sem_post(sem[0]); // E incrementa o sem 0 para a próxima pessoa poder entrar
				
                sem_wait(sem[1]); // Depois do processo entrar vai decrementar o sem 1 que inicia com valor 5
                
                sem_getvalue(sem[1], &lotacao); // Aqui guarda-se o valor do sem 1 após cada decrementação
                
                printf("Lotação: %d\n\n", lotacao);
                
                if(lotacao == 0){ // Quando o valor da lotacao for 0 significa que 5 processos já entraram no espetaculo
                
                    printf("LOTAÇÂO MÁXIMA!\n\n"); // Quando isso acontece pode-se começar o espetaculo
                    sem_wait(sem[0]); // Mas primeiro vou decrementar o sem 0 para não deixar que mais pessoas consigam entrar
                    sem_post(sem[2]); 
                }
					
				sem_wait(sem[3]);
				
				printf("--> O Visitante nrº%d acabou de sair!\n", i + 1);
                
                sem_post(sem[3]);
                
                sem_post(sem[1]); //incrementa para que novas pessoas possam entrar
                
                sem_getvalue(sem[1], &lotacao);
                
                if(lotacao == 5){
					printf("\n\n------------------------------------\n\n"); 
					sem_wait(sem[3]);
					sem_post(sem[0]);
				}
				
				exit(0); 
                
			}
		}
	}
	
	for(i = 0; i < 2; i++) {
        
        sem_wait(sem[2]);
		
		printf("\nO espetáculo nº%d está a decorrer!\n\n", i+1);
		
		sleep(5);
        
		printf("\nO espetáculo nº%d acabou!\n\n", i+1);
        
        sem_post(sem[3]); //em cima
	}
	
	sem_post(sem[2]);
	
	for(i = 0; i < 10; i++)
		wait(NULL);
	
	for(i = 0; i < 4; i++)
		sem_unlink(names[i]);

  return 0;
}
