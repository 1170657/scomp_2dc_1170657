#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <semaphore.h>

#define TAMANHO_BUFFER 10

typedef struct {
	int valores[TAMANHO_BUFFER];
	int head;
	int tail;
} array;

int main(){
	int i = 0, j = 0, k = 0, cont = 0;
	pid_t pid;
	
	sem_t *sem[3];	 // -> 3 SEMÁFOROS.
	char *names[3] = {"/sem_ex13_1","/sem_ex13_2","/sem_ex13_3"};
	int values[3] = {10, 0, 1};

	//----------------------Memória Patilhada----------------------------------
	int fd = shm_open("/shm_ex13", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);

	if(fd == -1){
		perror("Erro no shm_open!\n");
		exit(1);
	}

	if(ftruncate(fd, sizeof(array)) == -1){
		perror("Erro no ftruncate!\n");
		exit(1);
	}

	array * dados = (array *) mmap(NULL, sizeof(array), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	//-------------------------------------------------------------------------------------------
	
	//--------------------Criação de Semáforos--------------------------------------------------
	for(i = 0; i < 3; i++){
		if((sem[i]  = sem_open(names[i],  O_CREAT | O_EXCL, 0644, values[i])) == NULL){
			printf("Erro no sem_open()\n");
			sem_unlink(names[i]);
			exit(1);
		}
	}
	//-----------------------------------------------------------------------------------------
	
	//Cria 2 processos como dito no enunciado.
	for (i = 0; i < 2; i++){
		
		pid = fork();
		
		if(pid == 0){
			
			srand(time(NULL) % getpid());
			
			for (j = 0; j < 15; j++){
				
				//	Produtores ----- 10 0 1
				sem_wait(sem[0]);

				sem_wait(sem[2]);
				
				dados->valores[dados->head] = rand() % 100;
				dados->head = ((dados->head + 1) % TAMANHO_BUFFER);
				
				sem_post(sem[2]); // o sem2 é o semáforo que impede o outro filho de executar operações enquanto o primeiro não alterar o valor do buffer. Quando altera, dá permissão ao outro filho de alterar.			
				
				sem_post(sem[1]); // Após a posição ser alterada, o filho incrementa o semáforo que está a prender o pai.
				
				// --------
			}
			exit(0);
		}  else if (pid < 0){
			perror("Erro no fork\n");
			exit(1);
		}
	}

	//	Consumidor --------
	printf("\nCONSUMIDOR:\n\n");
	while (cont != 3){	   //	3 porque são 30 inteiros.
		
		sem_wait(sem[1]); // Este semáforo bloqueia o pai porque este é o leitor e não pode ler enquanto o filho não tiver escrito. Isto é feito, posição a posição.

		sem_wait(sem[2]); // Aqui também tem o sem2 que vai bloquear os filhos para eles não alterarem os valores do buffer enquanto o pai os está a ler.
		
		printf("Leitura posição: %d -> %d | Array: ", dados->tail % TAMANHO_BUFFER, dados->valores[dados->tail]);
		
		for (k = 0; k < 10; k++){
			if(k == 0){
				printf("[ %d, ", dados->valores[k]);
			} else if(k == 9){
				printf("%d]\n", dados->valores[k]);
			} else {
				printf("%d, ", dados->valores[k]);
			}
		}
		// Aqui o pai imprime todas as posições, estejam elas preenchidas ou não. As que forem preenchidas irão aparecer logo, se não tiverem preenchidas vão aparecer a 0 porque significa que os filhos ainda não as escreveram.
		
		dados->tail = ((dados->tail + 1) % TAMANHO_BUFFER);
		
		if (dados->tail == 0) {
			cont++;
		}
		
		sem_post(sem[2]);

		sem_post(sem[0]); // Só no loop do pai é que o sem 0 é incrementado, então o filho fica à espera que o pai chegue aqui .
	}

	if(munmap(dados, sizeof(array)) == -1){
		perror("Erro no munmap\n");
		exit(1);
	}

	if(close(fd) == -1){
		perror("Erro no close\n");
		exit(1);
	}
	
	if(shm_unlink("/shm_ex13") == -1) {
		perror("Erro no shm_unlink\n");
		exit(1);
	}

	for(i = 0; i < 3; i++){
		sem_unlink(names[i]);
	}

    return 0;
}
