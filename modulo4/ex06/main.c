#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <wait.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>

int main(){
	pid_t pid;
	int i = 0;
	sem_t *sem[2];
	char * names[2] = {"/sem_ex05_1", "/sem_ex05_2"};

	for(i = 0; i < 2; i++){
		if((sem[i] = sem_open(names[i], O_CREAT | O_EXCL, 0644, 0)) == SEM_FAILED){
			printf("Erro no sem_open()\n");
			sem_unlink(names[i]);
			exit(1);
		}
	}
	
	sem_post(sem[0]);
	
	pid = fork();
	
	for(i = 0; i < 15; i++){
		if(pid == 0){
			sem_wait(sem[0]);
			printf("Mensagem: %d - Eu sou o filho!\n", i);
			sem_post(sem[1]);
		} else { 	
			sem_wait(sem[1]);
			printf("Mensagem: %d - Eu sou o pai!\n", i);
			sem_post(sem[0]);
		}
	}
	
	for(i = 0; i < 2; i++){
		sem_unlink(names[i]);
	}
	return 0;
}
