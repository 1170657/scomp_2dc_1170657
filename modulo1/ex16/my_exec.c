#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

void my_exec(char* command) {
	int ret;
    ret = execlp (command, command, (char*)NULL);
    exit(ret);
}
