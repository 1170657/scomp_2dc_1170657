#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include "my_exec.h"

void function(char* command){ 
	int status;
	pid_t pid;
    
	pid = fork();
		
	if(pid == 0){
		my_exec(command);
	} else {
		wait(&status);
        if (WIFEXITED(status)) {
			int exit_status = WEXITSTATUS(status); 					
			printf("Exit status: %d\n", exit_status);
		}
	}
}

