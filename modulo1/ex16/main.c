#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include "function.h"

int main(){
	
	printf("\n| COMANDO LS: |\n");
	char* command1 = "ls";
	function(command1);
	printf("\n");
	
	printf("\n| COMANDO PS: |\n");
	char* command2 = "ps";
	function(command2);
	printf("\n");
	
	printf("\n| COMANDO WHO: |\n");
	char* command3 = "who";
	function(command3);
	printf("\n");
	
 return 0;
}
