#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
	int status;
	int i;
	pid_t pids[2];
	
	pids[0] = fork();
	
	if(pids[0] == 0) {
		sleep(1);
		exit(1);
	}
	
	pids[1] = fork();
	
	if(pids[1] == 0) { 
		sleep(2);
		exit(2);
	}
	
	for(i=0; i<2; i++) {
		waitpid(pids[i], &status, 0);
		if(WIFEXITED(status)) {
			int exit_status = WEXITSTATUS(status);
			printf("Valor de saída do Processo filho%d: %d\n", i+1, exit_status);
		}
	}
	
	return 0;
}
