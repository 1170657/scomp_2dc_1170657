#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
	int i, j;
	pid_t pids[4];
	
	for(i = 0; i < 4; i++){ 
		pids[i] = fork();
        if(pids[i] == 0){ 
            printf("PID do filho: %d | PID do pai: %d\n",getpid(),getppid()); 
            exit(0); 
        } 
    } 
    
    for(j = 0; j < 4; j++){
		if(pids[i] % 2 == 0) {
			wait(NULL); 
		}
	}

	return 0; 
}
