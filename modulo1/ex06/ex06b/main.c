#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
	int i, j;
	
	for(i = 0; i < 4; i++){ 
        if(fork() == 0){ 
            printf("PID do filho: %d | PID do pai: %d\n",getpid(),getppid()); 
            exit(0); 
        } 
    } 
    
    for(j = 0; j < 4; j++){
		wait(NULL); 
	}

	return 0; 
}
