#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
	
    srand(time(NULL));
    
    pid_t pids[10];
	int numbers[2000];
	int i, j, y, numero, pos, start = 0, end = 0;
    
    for(i = 0; i < 2000; i++) { 
        numbers[i] = rand()%2000;
    }
    
    numero = rand()%2000;
    
    for(i = 0; i < 10; i++) { 
        start = i * 200;
        end = (i + 1) * 200; 
        
        pids[i] = fork();
        
        if(pids[i] == 0) { 
            for(y = start; y < end; y++) {
                if(numbers[y] == numero) {
                    pos = 200 - (end - y);
                    exit(pos);
				}
			}
            exit(255); 
        }
    }
    
    for(j = 0; j < 10; j++) { 
        int status;
        wait(&status);
        if (WIFEXITED(status)) { 
            int exit_status = WEXITSTATUS(status); 
            if(exit_status != 255) {
                printf("Índice onde o número é encontrado: %d\n", exit_status+100);
            }
        }
    }
    
    return 0;
}
