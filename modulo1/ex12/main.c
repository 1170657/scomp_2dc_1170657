#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
	int i;
	int n=6;
	int indice;

		indice = spawn_childs(n)*2;
		if (indice != 0) {
		  exit(indice);
		} else {
			for(i = 1; i < 7; i++) {
				int status;
				wait(&status);
				if (WIFEXITED(status)) { 
					int exit_status = WEXITSTATUS(status);
					printf("O filho %d retorna o Índice %d!\n", (exit_status/2), exit_status);
				}
			}
			
			printf("O Pai retorna o Índice %d!\n", (indice/2), indice);
		}
		
return 0;
}
	
