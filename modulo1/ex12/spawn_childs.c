#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int spawn_childs(int n) {
	int i;
	
	pid_t pids[n];
	
	for(i = 1; i < n+1; i++) {  
        
        pids[i] = fork();
        
        if(pids[i] == 0) { 
            return i;
        } 
    }
   return 0;
}
