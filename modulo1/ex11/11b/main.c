#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
    srand(time(NULL));
    
    int status;
    
    pid_t pids[5], p;
    
	int numbers[1000];
    int result[1000];
    
	int k, i, y, start = 0, end = 0, max, max_value = 0;
	
    int resultLength = sizeof(result)/sizeof(int);
    
    for(i = 0; i < 1000; i++) { 
        numbers[i] = rand()%255;
    }
    
    for(i = 0; i < 5; i++) {
        start = i * 200;
        end = (i + 1) * 200; 
        
        pids[i] = fork();
        
        if(pids[i] == 0) { 
            max = 0;
            for(y = start; y < end; y++) {
                if(numbers[y] > max) {
                    max = numbers[y];
                }
            }
            printf("Valor Máximo: %d\n\n", max);
            exit(max); 
        }
    }
    
    for(i = 0; i < 5; i++) { 
        wait(&status);
        if (WIFEXITED(status)) { 
            int exit_status = WEXITSTATUS(status); 
            if(exit_status > max_value) {
                max_value = exit_status;
            }
        } 
    }
    
    printf("Máximo valor do array: %d\n", max_value);
    
    p = fork();
    
    if(p == 0) {
        printf("Metade do filho: { ");
        for(k = 0; k < (resultLength) / 2; k++){
            result[k] = ((int) numbers[k]/max_value) * 100;
            
            if(k == ((resultLength) / 2) - 1){
                printf(" %d}\n", result[k]);
            } else {
                printf("%d, ", result[k]);
            }
        }
        exit(0);
    } else if(p > 0){
        wait(&status);
    } 
    
    return 0;
}
