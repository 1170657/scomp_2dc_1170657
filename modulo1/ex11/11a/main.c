#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
    srand(time(NULL));
    
    pid_t pids[5];
	int numbers[1000];
	
	int i, y, start = 0, end = 0, max;
    
    for(i = 0; i < 1000; i++) { 
        numbers[i] = rand()%255;
    }
    
    for(i = 0; i < 5; i++) { 
        start = i * 200;
        end = (i + 1) * 200; 
        
        pids[i] = fork();
        
        if(pids[i] == 0) { 
            max = 0;
            for(y = start; y < end; y++) {
                if(numbers[y] > max) {
                    max = numbers[y];
                }
            }
            printf("Valor Máximo: %d\n\n", max);
            exit(max); 
        }
    }
    
    for(i = 0; i < 5; i++) { 
        int status;
        wait(&status);
        if (WIFEXITED(status)) { 
            continue;
        } 
    }
    
    return 0;
}
