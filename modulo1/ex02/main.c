#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
	
	int status;
	int i; 
	
	pid_t pids[3];

	printf("I'm...\n");
	
	pids[0] = fork();
	
	if(pids[0] > 0){
		printf("The...\n");
		pids[1] = fork();
	}
	
	if(pids[1] > 0){
		printf("Father!\n");
		pids[2] = fork();
	}
	
	if (pids[0] == 0 || pids[1] == 0 || pids[2] == 0) { 
		printf("I'll never join you!\n");
	} else {
		for(i=0; i<3; i++) {
			waitpid(pids[i], &status, 0);
			if(WIFEXITED(status)) {
				continue;
			}
		}
	}

	return 0;
}
