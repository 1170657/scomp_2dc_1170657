#include <stdio.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

int main(){
    int ret, status;
    pid_t filho1;
    char caminhoComando[100];
    
    printf("Insira o caminho que pretende executar\n");
	scanf("%s", caminhoComando);
    
    filho1 = fork();
    
    if(filho1 == 0) {
		ret = execlp (caminhoComando, "ls", (char*)NULL);
		exit(ret);
	} else { 
		wait(&status);
        if (WIFEXITED(status)) {
			int exit_status = WEXITSTATUS(status); 	
			if (exit_status == 0) {				
				printf("Deu bem!!!\n");
				printf("Exit status: %d\n", exit_status);
			} else { 
				printf("Deu mal!!!\n");
				printf("Exit status: %d\n", exit_status);
			}
			
		} 
	}
	return 0;
}
