#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
	int array_size = 1000;
	int numbers[array_size];
	int n;
	time_t t;
	int i, k, j;
	int cont=0;
	srand ((unsigned) time (&t));
	int status;
	
	for(i = 0; i < array_size; i++) {
		numbers[i] = rand()%100;
	}
	
	n = rand()%100;
	
	pid_t filho1=fork();
	
	if(filho1 != 0) { 
		for(j=0; j<=500; j++) {
			if(numbers[i] == n) {
				cont++;
			}
		}
	}else { 
		for(k=501; k<1000; k++) {
			if(numbers[k] == n) {
				cont++;
			}
		}
		exit(cont);
	}
	
		wait(&status);
		if(WIFEXITED(status)) {
			int exit_status = WEXITSTATUS(status);
			int contTotal = cont + exit_status;
			printf("Resultado: %d\n", contTotal);
		}
	
	return 0;
}
