#include <stdio.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#define BUFFER_SIZE 80

int main(void){

	char read_msg[BUFFER_SIZE];
	char write_msg[BUFFER_SIZE] = "Funciona!";
	int fd[2];
	pid_t filho;

	/* cria o pipe */
	if(pipe(fd) == -1){
		return 1;
	}

	filho = fork(); 

	if(filho != 0) {
		printf("PID do Processo Pai: %d\n", getpid());
		close(fd[0]);
		write(fd[1], write_msg, strlen(write_msg)+1);
		close(fd[1]);
	} else {
		int status;
        wait(&status);
        if (WIFEXITED(status)) { 
            close(fd[1]);
			read(fd[0], read_msg, BUFFER_SIZE);
			printf("\nFilho leu: %s\n", read_msg);
			close(fd[0]);
        }
	}
return 0;
}
