#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void) {

	pid_t pids[10];
	int status;
	int i, j, k;
	int start=0;
	int end=0;
	
	for(i=0; i<=10; i++) {
		start = i*100 + 1;
		end = (i+1)*100 +1;
		
		pid_t filho = fork();
		pids[i] = filho; 
		
		if(filho == 0) {
			for(j=start; j<end; j++) {
				printf("%d\n", j);
			}
			exit(1);
		}
	}
	
	for(k=0; k<=10; k++) {
		waitpid(pids[k], &status, 0);
		if(WIFEXITED(status)) {
			continue;
		}
	}
	
	/*Resposta à pergunta 9.(a): Não podemos garantir que seja ordenado por fatores como o escalonamento, 
	 * concorrência de processos... Não há forma de garantir qual processo termina primeiro, por exemplo.*/
	
	return 0;
	
	
}

